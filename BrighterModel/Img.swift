//
//  Img.swift
//  BrighterModel
//
//  Created by vikingosegundo on 07/10/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

public enum Img {
    case src(_Source); public enum _Source {
        case system(String)
    }
}
