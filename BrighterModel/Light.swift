//
//  Light.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 12.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

public func alter(_ light:Light, by changes:Light.Change...) -> Light { changes.reduce(light) { $0.alter(by: $1) } }

public func hueValue        (of light:Light     ) -> Double     { light.hue                          }
public func saturationValue (of light:Light     ) -> Double     { light.saturation                   }
public func brightnessValue (of light:Light     ) -> Double     { light.brightness                   }
public func temperatureValue(of light:Light     ) -> Int        { light.ct                           }
public func idValue         (of light:Light     ) -> String     { light.id                           }
public func nameValue       (of light:Light     ) -> String     { light.name                         }
public func modeValue       (of light:Light     ) -> Light.Mode { light.selectedMode                 }
public func systemImage     (for mode:Light.Mode) -> Img        { .src(.system(imageName(for:mode))) }

public struct Light:Codable, Equatable, Identifiable {
    public enum Change {
        case renaming(_RenameIt); public enum _RenameIt { case it     (to:String)    }
        case turning (_TurnIt  ); public enum _TurnIt   { case it     (Turn)         }
        case adding  (_Add     ); public enum _Add      { case mode   (Light.Mode)   }
        case toggling(_Toggle  ); public enum _Toggle   { case display(to:Interface) }
        case setting (_Set     ); public enum _Set      {
            case hue        (to:Double     )
            case saturation (to:Double     )
            case brightness (to:Double     )
            case temperature(to:Temperature)
        }
    }
    public init(id:String, name:String) { self.init(id, name, .off, 0, 0, 0, 0, .slider, [], .unset) }

    public enum Mode:Codable { case unset, hsb, ct }
    public enum Temperature { case mirek(Int) }

    public let id        : String
    public let name      : String
    public let isOn      : Turn

    public let hue       : Double
    public let saturation: Double
    public let brightness: Double
    public let ct        : Int

    public let modes       : [Mode]
    public let selectedMode: Mode
    public let display     : Interface

    public func alter(by changes:Change...) -> Self { changes.reduce(self) { $0.alter($1) } }

    private init(_ x:String,_ n:String,_ o:Turn,_ b:Double,_ s:Double,_ h:Double,_ t:Int,_ d:Interface,_ m:[Mode],_ y:Mode) { id = x;name = n;isOn = o;hue = h;saturation = s;brightness = b;ct = t;modes = m;selectedMode = y;display = d }
    private func alter(_ change:Change) -> Self {
        func __(_ x: Double) -> Double { min(max(x, 0.0), 1.0) }
        func __(_ ct:Int) -> Int { min(max(ct, 153), 500)}
        switch change {
        case let .renaming(         .it(to:name      )): return .init(id, name, isOn,   brightness,   saturation,   hue,   ct,    display, modes,          selectedMode)
        case     .turning (         .it(.on          )): return .init(id, name, .on,    brightness,   saturation,   hue,   ct,    display, modes,          selectedMode)
        case     .turning (         .it(.off         )): return .init(id, name, .off,   brightness,   saturation,   hue,   ct,    display, modes,          selectedMode)
        case let .setting ( .brightness(to:brightness)): return .init(id, name, isOn,__(brightness),  saturation,   hue,   ct,    display, modes,          selectedMode)
        case let .setting ( .saturation(to:saturation)): return .init(id, name, isOn,   brightness,__(saturation),  hue,   ct,    display, modes,          .hsb        )
        case let .setting (        .hue(to:hue       )): return .init(id, name, isOn,   brightness,   saturation,__(hue),  ct,    display, modes,          .hsb        )
        case let .setting (.temperature(to:.mirek(ct))): return .init(id, name, isOn,   brightness,   saturation,   hue,__(ct),   display, modes,          .ct         )
        case     .toggling(    .display(to:.slider   )): return .init(id, name, isOn,   brightness,   saturation,   hue,   ct,    .slider, modes,          selectedMode)
        case     .toggling(    .display(to:.scrubbing)): return .init(id, name, isOn,   brightness,   saturation,   hue,   ct, .scrubbing, modes,          selectedMode)
        case let .adding  (       .mode(mode         )): return .init(id, name, isOn,   brightness,   saturation,   hue,   ct,    display, modes + [mode], selectedMode)
        }
    }
}
private
func imageName(for mode:Light.Mode) -> String {
    mode == .hsb
        ? "paintpalette"
        : mode  == .ct
            ? "thermometer"
            : "exclamationmark.octagon" // uns  et
}
