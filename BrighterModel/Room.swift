//
//  Room.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 21.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

public
struct Room: Equatable, Codable, Identifiable {
    public enum Change { case title(String) }
    
    public init(id: String, title:String) {
        self.id = id
        self.title = title
    }
    
    public let id: String
    public let title:String
    
    public func alter(_ change: Change) -> Self { switch change { case .title(let title): return .init(id: id, title: title) } }
}
