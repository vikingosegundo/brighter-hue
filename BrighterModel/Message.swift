//  Copyright © 2021 Manuel Meyer. All rights reserved.


//MARK: - Message
public enum Message {
    case lighting(_Lighting)
    case dashboard(_Dashboard)
    case logging(_Logging)
}

//MARK: - Common Lighting DSL Elements
public enum Outcome { case succeeded, failed(Error?)        }
public enum Items   { case lights, rooms                    }
public enum Loaded  { case lights([Light]), rooms([Room])   }
public enum Turn    { case on, off                          }
public enum Apply   { case values(Light.Values)             }
public enum Alter   { case name(String), display(Interface) }

public enum Interface: Codable  { case scrubbing, slider }

public extension Double { var pt   : Light.Value.Increment { .pt   (self) } }
public extension Int    { var mirek: Light.Temperature     { .mirek(self) } }
public extension Int    { var mk   : Light.Temperature     { .mirek(self) } }
public extension Light  {
    enum Value {
        case hue, saturation, brightness, colortemp
        public enum Increment { case pt(Double) }
    }
    enum Values { case hsb(Double, Double, Double), ct(Int, Double), bri(Double) }
}
//MARK: - Lighting Message
public extension Message {
    enum _Lighting { // Lighting Feature
    //       |------------------------ Command ----------------------|  |---------------------------- Response ----------------------------|
        case load    (Items                                          ), loading   (Loaded,                                          Outcome)
        case turn    (Light,    Turn                                 ), turning   (Light,    Turn,                                  Outcome)
        case apply   (Apply,                                 on:Light), applying  (Apply,                                 on:Light, Outcome)
        case change  (Alter,                                 on:Light), changing  (Alter,                                 on:Light, Outcome)
        case increase(Light.Value, by:Light.Value.Increment, on:Light), increasing(Light.Value, by:Light.Value.Increment, on:Light, Outcome)
        case decrease(Light.Value, by:Light.Value.Increment, on:Light), decreasing(Light.Value, by:Light.Value.Increment, on:Light, Outcome)
    }
}
public extension Message {
    enum _Dashboard { // Dashboard Feature
    //       |--- Command --|  |---------- Response ------------|
        case add   (Favorite), favoriteAdded  (Favorite, Outcome)
        case remove(Favorite), favoriteRemoved(Favorite, Outcome)
    }
}
public extension Message {
    enum _Logging { // Logging Feature
    //      |-- Command --|
        case write(LogItem)
    }
}
extension Turn:Codable {}
extension Light.Value.Increment: Equatable {}
