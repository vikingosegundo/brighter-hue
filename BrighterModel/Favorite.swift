//
//  Favorite.swift
//  BrighterModel
//
//  Created by vikingosegundo on 02/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

public
struct Favorite:Codable, Equatable {
    public
    enum Kind:Codable {
        case switchOnOff
    }
    public
    init(kind:Kind, lightID:String) {
        self.kind = kind
        self.lightID = lightID
    }
    public let kind:Kind
    public let lightID:String
}
