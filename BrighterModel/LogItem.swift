//
//  LogItem.swift
//  BrighterModel
//
//  Created by vikingosegundo on 24/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

public struct LogItem {
    public let text:String
    public let date:Date
    
    public init (text t:String, date d:Date = Date()) { text = t; date = d }
}
