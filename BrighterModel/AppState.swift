//
//  AppState.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//

import BrighterTools

public struct AppState: Codable {
    public enum Change {
        case by(_By); public enum _By {
            case adding(_Add); public enum _Add {
                case light(Light)
                case favorite(Favorite)
            }
            case replacing(_Replace); public enum _Replace {
                case lights(with:[Light])
                case light (with: Light )
                case rooms (with:[Room ])
            }
            case removing(_Remove); public enum _Remove {
                case favorite(Favorite)
            }
        }
    }
    public  func alter(_ changes:[ Change ] ) -> Self { changes.reduce(self) { $0.alter($1) } }
    private func alter(_ change :  Change   ) -> Self {
        func sort  (_                    lights:[Light]) -> [Light] { lights.sorted(on:\.id, by:<)     }
        func remove(_ light: Light, from lights:[Light]) -> [Light] { lights.filter{$0.id != light.id} }
        switch change {
        case let .by(.adding   (.light (light ))): return .init(sort(lights                     + [light]), rooms, favorites)
        case let .by(.replacing(.lights(lights))): return .init(     lights                               , rooms, favorites)
        case let .by(.replacing(.light (light ))): return .init(sort(remove(light, from:lights) + [light]), rooms, favorites)
        case let .by(.replacing(.rooms (rooms ))): return .init(     lights                               , rooms, favorites)
        case let .by(.adding   (.favorite(fav ))): return .init(     lights                               , rooms, favorites + [fav])
        case let .by(.removing (.favorite(fav ))): return .init(     lights                               , rooms, favorites.filter({ fav.lightID != $0.lightID }))
        }
    }
    public init() {
        self.init([], [], [])
    }
    private
    init(_ lights: [Light],_ rooms: [Room],_ favorites: [Favorite]) {
        self.lights = lights
        self.rooms = rooms
        self.favorites = favorites
    }
    public let lights: [Light]
    public let rooms : [Room ]
    public let favorites: [Favorite]
}
public func lights(in state:AppState) -> [Light] { state.lights }
