//
//  ContentView.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import BrighterModel

public struct ContentView: View {
    public init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState   = viewState
        self.rootHandler = rootHandler
    }
    public var body: some View {
        TabView(selection:$tab) {
            Dashboard(rootHandler).tabItem { Label(name(for:0), systemImage:imageName(for:0)) }.tag(0)
            Lights   (rootHandler).tabItem { Label(name(for:1), systemImage:imageName(for:1)) }.tag(1)
            Rooms    (rootHandler).tabItem { Label(name(for:2), systemImage:imageName(for:2)) }.tag(2)
            Shop     (rootHandler).tabItem { Label(name(for:3), systemImage:imageName(for:3)) }.tag(3)
            Settings (rootHandler).tabItem { Label(name(for:4), systemImage:imageName(for:4)) }.tag(4)
        }
        .onChange(of:tab) { actionFor(tap: $0) }
        .onAppear         { actionFor(tap:tab) }
        .accentColor(tabColors[tab])
        .environmentObject(viewState)
    }
    @State private var tab : Int = 0
    @ObservedObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
    
    fileprivate func actionFor(tap idx:Int) {
        viewState.themeColor = tabColors[idx]
        switch idx {
        case 0..<1: rootHandler(.lighting(.load(.lights)))
        default   : ()
        }
    }
}

//MARK: - private data sources
fileprivate let tabColors = (0..<5)
    .map { Color(       hue:hue       (at:$0),
                 saturation:saturation(at:$0),
                 brightness:brightness(at:$0))
    }.shuffled()
fileprivate func hue       (at idx:Int) -> Double { switch idx { case 0: return 0.305; case 1: return 0.511; case 2: return 0.602; case 3: return 0.755; case 4: return 0.111; default: return 0.0 } }
fileprivate func saturation(at idx:Int) -> Double { switch idx { case 0: return 0.45;  case 1: return 0.75;  case 2: return 0.75;  case 3: return 0.55;  case 4: return 0.50;  default: return 0.0 } }
fileprivate func brightness(at idx:Int) -> Double { 0.75 }
fileprivate func imageName(for idx:Int) -> String {
    switch idx {
    case  0: return "slider.horizontal.below.square.fill.and.square"
    case  1: return "lightbulb"
    case  2: return "bed.double"
    case  3: return "cart"
    case  4: return "gear"
    default: return "rectangle"
    }
}
fileprivate func name(for idx:Int) -> String {
    switch idx {
    case  0: return "Dashboard"
    case  1: return "Lights"
    case  2: return "Rooms"
    case  3: return "Shop"
    case  4: return "Settings"
    default: return ""
    }
}
