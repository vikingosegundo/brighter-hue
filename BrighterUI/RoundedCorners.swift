//
//  RoundedCorners.swift
//  RoundedCorners
//
//  Created by vikingosegundo on 27/07/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//
import SwiftUI

struct RoundedCorners: Shape {
    /**
     * tl: top left
     * tr: top right
     * bl: bottom left
     * br: bottom right */
    init(tl: Double = 0, tr: Double = 0, bl: Double = 0, br: Double = 0) { self.tl = tl; self.tr = tr; self.bl = bl; self.br = br }
    
    func path(in rect: CGRect) -> Path {
        let (w,h) = (rect.size.width, rect.size.height)
        var p = Path()
        let angles = angles(generator: create90DegreeAngleGenerator())
        p.move   (    to: .init(x:     tl, y:   0   ))
        p.addLine(    to: .init(x: w - tr, y:   0   ))
        p.addArc (center: .init(x: w - tr, y:     tr), radius:tr, startAngle:angles.top   .right.start, endAngle:angles.top   .right.end, clockwise:false)
        p.addLine(    to: .init(x: w     , y: h - br))
        p.addArc (center: .init(x: w - br, y: h - br), radius:br, startAngle:angles.bottom.right.start, endAngle:angles.bottom.right.end, clockwise:false)
        p.addLine(    to: .init(x:     bl, y: h     ))
        p.addArc (center: .init(x:     bl, y: h - bl), radius:bl, startAngle:angles.bottom.left .start, endAngle:angles.bottom.left .end, clockwise:false)
        p.addLine(    to: .init(x:   0   , y:     tl))
        p.addArc (center: .init(x:     tl, y:     tl), radius:tl, startAngle:angles.top   .left .start, endAngle:angles.top   .left .end, clockwise:false)
        return p
    }
    private let tl, tr, bl, br:Double
}

fileprivate
func angles(generator g:() -> Angle) -> (top:(left:(start:Angle, end:Angle), right:(start:Angle, end:Angle)), bottom:(left:(start:Angle, end:Angle), right:(start:Angle, end:Angle))) {
    let x = (
        bottomRight:(g(), g()),
        bottomLeft :(g(), g()),
        topLeft    :(g(), g()),
        topRight   :(g(), g())
    )
    return (
        top:(
            left: x.topLeft,
            right:x.topRight),
        bottom:(
            left: x.bottomLeft,
            right:x.bottomRight)
    )
}
fileprivate
enum Truncation {
    enum Options: Equatable{
        case no
        case yes
        case at(Double)
    }
}
fileprivate
func create90DegreeAngleGenerator(truncating:Truncation.Options = .yes) -> () -> Angle {
    var p = false
    var start = 0.0 { didSet { p = !p } }
    return {
        start = (start + (90 * (p ? 1.0 : 0.0))).truncatingRemainder(dividingBy: 360)
        switch truncating {
        case .no           : return .degrees(start)
        case .yes          : return .degrees(start.truncatingRemainder(dividingBy:360.0))
        case .at(let value): return .degrees(start.truncatingRemainder(dividingBy:value))
        }
    }
}
