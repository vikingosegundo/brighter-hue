//
//  Shop.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 18.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import BrighterModel

struct Shop: View {
    init(_ rootHandler: @escaping (Message) -> ()) {
        self.rootHandler = rootHandler
    }
    var body: some View {
        VStack {
            NavigationView {
                List {
                }.buttonStyle(PlainButtonStyle()).navigationTitle("Shop")
            }
        }
    }
    @EnvironmentObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
}
