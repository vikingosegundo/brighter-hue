//
//  Rooms.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 18.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import BrighterModel

struct Rooms:View {
    init(_ rootHandler: @escaping (Message) -> ()) {
        self.rootHandler = rootHandler
    }
    var body: some View {
        VStack {
            NavigationView {
                List {
                    ForEach(viewState.rooms) { RoomCell($0,rootHandler) }
                }.buttonStyle(PlainButtonStyle()).navigationTitle("Rooms")
            }
        }
    }
    @EnvironmentObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
}

struct RoomCell:View {
    init(_ room: Room, _ rootHandler: @escaping (Message) -> ()) {
        self.rootHandler = rootHandler
        self.room = room
    }
    var body: some View {
        Text(room.title)
    }
    private let rootHandler: (Message) -> ()
    private let room: Room
}
