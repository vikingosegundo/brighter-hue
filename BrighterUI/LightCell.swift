//
//  LightCell.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 02.07.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import UIKit.UIImage
import BrighterModel

struct LightCell: View {
    init(light: Light, rootHandler: @escaping (Message) -> ()) {
        self.rootHandler  = rootHandler
        self.lightID      = light.id
        self.isOn         = light.isOn == .on
        self.hue          = hueValue       (of:light)
        self.sat          = saturationValue(of:light)
        self.bri          = brightnessValue(of:light)
        self.ct           = Double(-temperatureValue(of:light))
        self.interface    = light.display
        self.selectedMode = light.modes.contains(.hsb) ? .hsb : .ct
        self.values       = light.modes.contains(.hsb) ? .hsb(hueValue(of:light), saturationValue(of:light), brightnessValue(of:light))
                                                       : .ct(temperatureValue(of:light),                     brightnessValue(of:light))
    }
    var body: some View {
        cellLayout
            .actionSheet(isPresented: $showingOptions) {
                let lightInFavorites: (Favorite.Kind) -> Bool = { viewState.favorites.contains(.init(kind:$0, lightID: light.id)) }
                return ActionSheet(title: Text(light.name),
                                   message: Text("Perform action"),
                                   buttons: [
                                    .default(
                                        Text(
                                            !lightInFavorites(.switchOnOff)
                                                ?    "add «\(light.name)» to favorites"
                                                : "remove «\(light.name)» from favorites")
                                    ) { rootHandler(.dashboard(
                                        !lightInFavorites(.switchOnOff)
                                            ? .add   (.init(kind:.switchOnOff, lightID:light.id))
                                            : .remove(.init(kind:.switchOnOff, lightID:light.id)))) },
                                    .cancel() ]
                )
            }
            .frame   (      minHeight:isCollapsed ? height.collapsedCell : height.uncollapsedCell)
            .modifier( HeightModifier(isCollapsed ? height.collapsedCell : height.uncollapsedCell))
            .swipeActions(edge: .leading ) { toggleColorMode }
            .swipeActions(edge: .trailing) { toggleInterface }
            .listRowBackground( bgColor )
            .tint(hsbColor(opacity: 0.5))
            .padding(.top,    15)
            .padding(.bottom, 15)
            .buttonStyle(PlainButtonStyle())
            .onChange(of:light.ct        ) { ct  = Double(-$0); values = .ct (-$0,                                           brightnessValue(of:light)) }
            .onChange(of:light.hue       ) { hue =         $0 ; values = .hsb(-$0,                saturationValue(of:light), brightnessValue(of:light)) }
            .onChange(of:light.saturation) { sat =         $0 ; values = .hsb(hueValue(of:light), $0,                        brightnessValue(of:light)) }
            .onChange(of:light.brightness) { bri =         $0 ; values = .hsb(hueValue(of:light), saturationValue(of:light), $0                       ) }
    }
    @EnvironmentObject private var viewState: ViewState
    @State private var values       : Light.Values
    @State private var isOn         : Bool
    @State private var hue          : Double
    @State private var sat          : Double
    @State private var bri          : Double
    @State private var ct           : Double
    @State private var interface    : Interface
    @State private var startPoint   : CGPoint?
    @State private var currentPoint : CGPoint?
    @State private var isCollapsed  : Bool = true
    @State private var selectedMode : Light.Mode
    @State private var showingOptions = false
    private let rootHandler: (Message) -> ()
    private let lightID    : String
    private var light      : Light { viewState.lights.first(where: { $0.id == lightID}) ?? Light(id: "-1", name: "never") }
}
private extension LightCell {
    //MARK: - Interactions
    private func turn    (_ light:Light, _ onOrOff:Turn                                     ) { rootHandler( .lighting(.turn(light,onOrOff))             ) }
    private func apply   ( values:Light.Values,                               on light:Light) { rootHandler( .lighting(.apply(.values(values),on:light)) ) }
    private func increase( value :Light.Value,  by inc:Light.Value.Increment, on light:Light) { rootHandler( .lighting(.increase(value,by:inc,on:light)) ) }
    private func decrease( value :Light.Value,  by dec:Light.Value.Increment, on light:Light) { rootHandler( .lighting(.decrease(value,by:dec,on:light)) ) }
    
    private func setHueAndSaturationFor(distanceX:CGFloat, distanceY:CGFloat) {
        apply(values:
                    .hsb(
                        min(max(hueValue(of:light)         + distanceX / 4096, 0.0), 1.0),
                        min(max(saturationValue(of:light)  - distanceY / 1024, 0.0), 1.0),
                        brightnessValue(of:light)),
              on:light)
    }
    private func setCTfor(distanceX: CGFloat, distanceY:CGFloat) {
        apply(values:
                    .ct(
                        temperatureValue(of:light) + Int(distanceX / 4),
                        brightnessValue (of:light)                    ),
              on:light)
    }
    //MARK: - Views
    private var cellLayout: some View {
        VStack {
            cellheader
            if (light.modes.contains(.hsb) || light.modes.contains(.ct)) {
                VStack {
                    switch (selectedMode, light.display) {
                    case (.ct, .slider):
                        Spacer()   .collapse(isCollapsed)
                        ctSliderRow.collapse(isCollapsed)
                    case (.hsb, .slider):
                        Spacer()    .collapse(isCollapsed)
                        hueSliderRow.collapse(isCollapsed)
                        satSliderRow.collapse(isCollapsed)
                    case (_, .scrubbing):
                        scrubbingView.collapse(isCollapsed)
                    case (.unset, _):
                        Spacer()
                    }
                }
                .opacity(isCollapsed ? 0.0 : 1.0)
            }
            briSliderRow
        }
    }
    private var toggleColorMode: some View {
        HStack {
            if !isCollapsed{
                let indexedModes = light
                    .modes
                    .filter { $0 != selectedMode }
                    .enumerated()
                    .map { (idx: $0.offset, mode: $0.element) }
                ForEach(indexedModes, id:\.idx) { i in
                    Button { selectedMode = i.mode } label: { imageFrom(img:systemImage(for:i.mode)) } } }
        }
    }
    private var toggleInterface: some View {
        HStack {
            Button {
                rootHandler(.lighting(.change(.display(light.display == .slider ? .scrubbing : .slider), on:light)))
            } label: { Label("", systemImage: light.display == .slider
                             ? "slider.horizontal.below.rectangle"
                             : "slider.horizontal.3") }
            Button("...") { showingOptions = true }
        }
    }
    private var cellheader: some View {
        HStack {
            Button {
                withAnimation { isCollapsed.toggle() }
            } label: {
                Image(systemName:"chevron.up")
                    .foregroundColor(hsbColor(opacity:0.75))
                    .contentShape(Rectangle())
                    .rotationEffect( .radians(isCollapsed ? .pi / 2 : .pi))
            }
            titleLabel
                .multilineTextAlignment(.leading)
            Spacer()
            toggle
        }
        .rowStyle(height:height.row, isCollapsed: isCollapsed, position: .first)
    }
    private var titleLabel: some View {
        Text("\(nameValue(of:light))")
            .bold()
            .dynamicTypeSize(.xLarge)
            .fixedSize()
    }
    private var toggle: some View {
        Button { turn(light, light.isOn == .on ? .off : .on) } label: {
            Image(systemName: "togglepower")
                .renderingMode(.template)
                .foregroundColor(light.isOn == .on ? bgColor : .gray)
        }
    }
    private var scrubbingKnob: some View {
        HStack {
            Spacer()
            Circle()
                .fill(
                    LinearGradient(gradient: Gradient(colors: [hsbColor(saturation:1.0, opacity:0.5),
                                                               hsbColor(saturation:1.0, opacity:0.5),
                                                               hsbColor(saturation:0.1, opacity:0.5),
                                                               hsbColor(saturation:0.1, opacity:0.5)]),
                                   startPoint:.top, endPoint:.bottom)
                )}
        .frame(width:25, height:25)
        .offset(x:(currentPoint?.x ?? 0) - (startPoint?.x ?? 0), y:(currentPoint?.y ?? 0) - (startPoint?.y ?? 0))
        .gesture(
            DragGesture(
                minimumDistance: 5,
                coordinateSpace: .global
            ).onChanged {
                if let startpoint = startPoint {
                    switch selectedMode {
                    case .hsb:setHueAndSaturationFor(
                        distanceX: ( $0.location.x ) - (startpoint.x),
                        distanceY: ( $0.location.y ) - (startpoint.y))
                    case .ct: setCTfor(
                        distanceX: ( $0.location.x ) - (startpoint.x),
                        distanceY: ( $0.location.y ) - (startpoint.y))
                    case .unset: ()
                    }
                }
                switch startPoint == nil {
                case true : startPoint   = $0.location
                case false: currentPoint = $0.location
                }
            }.onEnded { _ in startPoint = nil; currentPoint = nil } ).padding(.bottom, 20)
    }
    private var scrubbingView: some View {
        ZStack {
            VStack {
                Spacer()
                scrubbingKnob
                Spacer()
            }
        }
        .contentShape(Rectangle())
    }
    private var ctSliderRow: some View {
        HStack {
            Button {
                increase(value:.colortemp, by: 10.pt, on:light)
            } label: {
                Image(systemName:"thermometer.snowflake")
                    .foregroundStyle(ctColor(ct:(Double(temperatureValue(of: light)) * 1.2)))
                    .accessibility(label:Text("increase color temperature"))
            }
            Slider(value: $ct, in: (-500)...(-153)) { _ in
                apply(values: .ct(Int(-ct),bri), on: light)
            }.accessibility(label:Text("change color temperature"))
            Button {
                decrease(value:.colortemp,by:10.pt,on:light)            } label: {
                    Image(systemName:"thermometer.sun")
                        .foregroundStyle(ctColor(ct:(Double(temperatureValue(of: light)) * 0.8)))
                        .accessibility(label:Text("decrease color temperature"))
                }
        }
        .rowStyle(height: height.row, isCollapsed: isCollapsed, position: .inbetween)
    }
    private var briSliderRow: some View {
        slider(
            bind: $bri,
            leading:
                    .init(action: { decrease(value:.brightness,by:10.pt,on:light) },
                          image: .system("sun.min"),
                          color: hsbColor(brightness: min(max(brightnessValue(of:light) - 0.05, 0.0), 1.0)),
                          accessibility: "decrease brightness"),
            slider:
                    .init(action: { apply(values:.bri(bri),on:light) },
                          accessibility:"change brightness"),
            trailing:
                    .init(action: { increase(value:.brightness,by:10.pt,on:light) },
                          image: .system("sun.max.fill"),
                          color: hsbColor(brightness: min(max(brightnessValue(of:light) + 0.05, 0.0), 1.0)),
                          accessibility: "increase brightness")
        ).rowStyle(height: height.row, isCollapsed: isCollapsed, position: .last).padding(.top, isCollapsed ? -16 : 0)
    }
    private var hueSliderRow: some View {
        slider(
            bind: $hue,
            leading:
                    .init(action: { decrease(value:.hue,by:10.pt,on:light)},
                          image: .system("paintpalette"),
                          color: hsbColor(hue: min(max(hueValue(of:light) - 0.05, 0.0), 1.0)),
                          accessibility: "decrease saturation"),
            slider:
                    .init(action: {  apply(values:.hsb(hue,sat,bri),on:light) },
                          accessibility: "change saturation"),
            trailing:
                    .init(
                        action: { increase(value:.hue,by:10.pt,on:light) },
                        image: .system("paintpalette"),
                        color: hsbColor(hue: min(max(hueValue(of:light) + 0.05, 0.0), 1.0)),
                        accessibility: "increase saturation")
        ).rowStyle(height: (selectedMode == .hsb && !isCollapsed) ? height.row : 0, isCollapsed: isCollapsed, position: .inbetween)
    }
    private var satSliderRow: some View {
        slider(
            bind: $sat,
            leading:
                    .init(action: { decrease(value:.saturation,by:10.pt,on:light) },
                          image: .system("circle.lefthalf.fill"),
                          color: hsbColor(saturation: min(max(saturationValue(of:light) - 0.25, 0.0), 1.0)),
                          accessibility: "decrease saturation"),
            slider:
                    .init(action: { apply(values:.hsb(hue,sat,bri),on:light) },
                          accessibility: "change saturation"),
            trailing:
                    .init(
                        action: { increase(value:.saturation,by:10.pt,on:light) },
                        image: .system("circle.righthalf.fill"),
                        color: hsbColor(saturation: min(max(saturationValue(of:light) + 0.25, 0.0), 1.0)),
                        accessibility: "increase saturation")
        ).rowStyle(height: (selectedMode == .hsb && !isCollapsed) ? height.row : 0, isCollapsed: isCollapsed, position: .inbetween)
        
    }
    private func imageForSlider(conf: SliderButtonConf.ButtonImage) -> Image { switch conf { case .system(let name): return Image(systemName: name) } }
    private func slider(
        bind: Binding<Double>,
        leading:SliderButtonConf, slider:SliderConf, trailing:SliderButtonConf) -> some View {
            HStack {
                Button {
                    leading.action()
                } label: {
                    imageForSlider(conf: leading.image)
                        .foregroundStyle(leading.color)
                        .accessibility(label:Text(leading.accessibility))
                }
                Slider(value: bind){ _ in
                    slider.action()
                }.accessibility(label:Text(slider.accessibility))
                Button {
                    trailing.action()
                } label: {
                    imageForSlider(conf: trailing.image)
                        .foregroundStyle(trailing.color)
                        .accessibility(label:Text(trailing.accessibility))
                }
            }
        }
    //MARK: - Colors
    private var bgColor: Color {
        switch values {
        case .hsb: return hsbColor(brightness: light.isOn == .on ? brightnessValue(of:light) : 0.1, opacity: light.isOn == .on ? 0.5 : 1.00)
        case .ct : return  ctColor(brightness: light.isOn == .on ? brightnessValue(of:light) : 0.1, opacity: light.isOn == .on ? 0.5 : 0.25)
        case .bri: return  ctColor(brightness: light.isOn == .on ? brightnessValue(of:light) : 0.1, opacity: light.isOn == .on ? 0.5 : 0.25)
        }
    }
    private func hsbColor(hue: Double? = nil, saturation:Double? = nil, brightness:Double? = nil, opacity: Double = 1.0) -> Color {
        Color(
            hue       : hue        != nil ? hue!        : hueValue       (of:light),
            saturation: saturation != nil ? saturation! : saturationValue(of:light),
            brightness: brightness != nil ? brightness! : brightnessValue(of:light),
            opacity   : opacity
        )
    }
    private func ctColor(ct: Double? = nil, brightness:Double? = nil, opacity: Double = 1.0) -> Color {
        Color(UIColor(kelvin: 1_000_000.0 / (ct != nil ? ct! : Double(temperatureValue(of:light)))))
            .opacity(opacity)
    }
}
//MARK: - Misc
fileprivate struct SliderButtonConf {
    let action: () -> ()
    let image: ButtonImage
    let color: Color
    let accessibility:String
    enum ButtonImage {
        case system(String)
    }
}
private extension LightCell {
    func selected(_ mode: Light.Mode)  { selectedMode =  mode }
}
fileprivate enum RowPositon { case first, last, inbetween }
fileprivate extension View {
    @ViewBuilder func rowStyle(height:Double, isCollapsed:Bool, position: RowPositon) -> some View {
        self.frame(height:height)
            .padding(.trailing, 15)
            .padding(.leading,  15)
            .background(material,
                        in: RoundedCorners(
                            tl: isCollapsed ? position == .first ? 5 : position == .inbetween ? 5 : 0 : 5,
                            tr: isCollapsed ? position == .first ? 5 : position == .inbetween ? 5 : 0 : 5,
                            bl: position == .last ? 5 : position == .inbetween ? 5 : (position == .first && isCollapsed) ? 0 : (position == .first && !isCollapsed) ? 5.0 : 0.0,
                            br: position == .last ? 5 : position == .inbetween ? 5 : (position == .first && isCollapsed) ? 0 : (position == .first && !isCollapsed) ? 5.0 : 0.0))
    }
    @ViewBuilder func collapse(_ isCollapsed:Bool) -> some View { self.opacity(isCollapsed ? 0 : 1).frame(maxHeight: isCollapsed ? 0 : .infinity) }
}
fileprivate struct HeightModifier: AnimatableModifier {
    init(_ height: Double) { self.height = height }
    var animatableData: Double {
        get { height }
        set { height = newValue }
    }
    func body(content: Content) -> some View { content.frame(height: height) }
    private var height = 0.0
}
fileprivate let height = (
    row: 36.0,
    collapsedCell: 60.0,
    uncollapsedCell: 180.0
)
fileprivate let material:Material = .thickMaterial
fileprivate func imageFrom(img:Img) -> Image { switch img { case .src(.system(let name)): return .init(systemName:name) } }
