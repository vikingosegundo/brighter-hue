//
//  Settings.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 18.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import BrighterModel

struct Settings: View {
    init(_ rootHandler: @escaping (Message) -> ()) {
        self.rootHandler = rootHandler
    }
    var body: some View {
        VStack {
            NavigationView {
                List {

                }.buttonStyle(PlainButtonStyle()).navigationTitle("Settings")
            }
        }
    }
    @EnvironmentObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
}
