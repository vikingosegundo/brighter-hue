//
//  Lights.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 18.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import BrighterModel

struct Lights: View {
    init(_ rootHandler: @escaping (Message) -> ()) {
        self.rootHandler = rootHandler
    }
    var body: some View {
        VStack {
            VStack {
                NavigationView {
                    List {
                        ForEach(Array(viewState.lights.enumerated()), id:\.offset) { LightCell(light: $1, rootHandler:rootHandler) }
                    }
                    .navigationTitle("Lights")
                }
            }
        }
    }
    @EnvironmentObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
}
