//
//  OnOffSwitchView.swift
//  BrighterUI
//
//  Created by vikingosegundo on 04/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import BrighterModel

struct OnOffSwitchView: View {
    init(light: Light, rootHandler: @escaping (Message) -> ()) {
        self.rootHandler = rootHandler
        self.lightID     = light.id
        self.bri         = brightnessValue(of:light)
        self.values      = light.modes.contains(.hsb) ? .hsb(hueValue(of:light), saturationValue(of:light), brightnessValue(of:light))
                                                      : .ct(temperatureValue(of:light),                     brightnessValue(of:light))
    }
    var body: some View {
        cellLayout
            .frame(height: 114)
            .tint(hsbColor(opacity: 0.5))
            .background(.black)
            .onChange(of: brightnessValue(of:light)) { bri = $0 ; values = .hsb(hueValue(of:light), saturationValue(of:light), $0 ) }
    }
    @EnvironmentObject private var viewState: ViewState
    @State private var values      : Light.Values
    @State private var bri         : Double

    private let rootHandler: (Message) -> ()
    private let lightID    : String
    private var light      : Light {
        viewState.lights.first(where: { $0.id == lightID }) ?? Light(id: "-1", name: "never")
    }
}
private extension OnOffSwitchView {
    //MARK: - Interactions
    private func turn    (_ light:Light, _ onOrOff:Turn                                    ) { rootHandler( .lighting(.turn(light,onOrOff))             ) }
    private func apply   ( values:Light.Values,                              on light:Light) { rootHandler( .lighting(.apply(.values(values),on:light)) ) }
    private func increase( value :Light.Value, by inc:Light.Value.Increment, on light:Light) { rootHandler( .lighting(.increase(value,by:inc,on:light)) ) }
    private func decrease( value :Light.Value, by dec:Light.Value.Increment, on light:Light) { rootHandler( .lighting(.decrease(value,by:dec,on:light)) ) }
    
    private func setHueAndSaturationFor(distanceX:CGFloat, distanceY:CGFloat) {
        apply(values:
                    .hsb(
                        min(max(       hueValue(of:light)  + distanceX / 4096, 0.0), 1.0),
                        min(max(saturationValue(of:light)  - distanceY / 1024, 0.0), 1.0),
                        brightnessValue(of:light)),
              on:light)
    }
    private func setCTfor(distanceX: CGFloat, distanceY:CGFloat) {
        apply(values:
                    .ct(
                        temperatureValue(of:light) + Int(distanceX / 4),
                        brightnessValue (of:light)                    ),
              on:light)
    }
    //MARK: - Views
    private var cellLayout: some View {
        ZStack {
            VStack {
                Color.init(white: 0.1)
                    .overlay(RoundedRectangle(cornerRadius: 5)
                                .stroke(light.isOn == .on ? bgColor : .init(white: 0.1), lineWidth: 3))
            }
            VStack {
                VStack {
                    cellheader.padding(.all, 10)
                    toggle
                    briSliderRow
                        .padding()
                        .disabled(light.isOn == .off)
                }
            }
        }
    }
    private var cellheader: some View {
        HStack {
            titleLabel.truncationMode(.middle)
        }
    }
    private var titleLabel: some View { Text("\(nameValue(of:light))").dynamicTypeSize(.xSmall) }
    private var toggle    : some View {
        Button { turn(light, light.isOn == .on ? .off : .on) } label: {
            Image(systemName:"togglepower")
                .renderingMode(.template)
                .foregroundColor(light.isOn == .on ? bgColor : .gray)
        }
    }
    private var briSliderRow: some View {
        stepper(
            bind: $bri,
            leading:
                    .init(action: { decrease(value:.brightness,by:10.pt,on:light) },
                          image: .system("sun.min"),
                          color: hsbColor(brightness: min(max(brightnessValue(of:light) - 0.05, 0.0), 1.0)),
                          accessibility:"decrease brightness"),
            trailing:
                    .init(action: { increase(value:.brightness,by:10.pt,on:light) },
                          image: .system("sun.max.fill"),
                          color: hsbColor(brightness: min(max(brightnessValue(of:light) + 0.05, 0.0), 1.0)),
                          accessibility:"increase brightness")
        )
    }
    private func imageForSlider(conf: SliderButtonConf.ButtonImage) -> Image { switch conf { case .system(let name): return Image(systemName: name) } }
    private func stepper(
        bind: Binding<Double>,
        leading:SliderButtonConf, trailing:SliderButtonConf) -> some View {
            HStack {
                Spacer()
                Button {
                    leading.action()
                } label: {
                    imageForSlider(conf: leading.image)
                        .foregroundStyle(leading.color)
                        .accessibility(label:Text(leading.accessibility))
                }
                Spacer()
                Button {
                    trailing.action()
                } label: {
                    imageForSlider(conf: trailing.image)
                        .foregroundStyle(trailing.color)
                        .accessibility(label:Text(trailing.accessibility))
                }
                Spacer()
            }
        }
    //MARK: - Colors
    private var bgColor: Color {
        switch values {
        case .hsb: return hsbColor(brightness: light.isOn == .on ? brightnessValue(of:light) : 0.1, opacity: light.isOn == .on ? 0.5 : 1.00)
        case .ct : return  ctColor(brightness: light.isOn == .on ? brightnessValue(of:light) : 0.1, opacity: light.isOn == .on ? 0.5 : 0.25)
        case .bri: return  ctColor(brightness: light.isOn == .on ? brightnessValue(of:light) : 0.1, opacity: light.isOn == .on ? 0.5 : 0.25)
        }
    }
    private func hsbColor(hue: Double? = nil, saturation:Double? = nil, brightness:Double? = nil, opacity: Double = 1.0) -> Color {
        Color(
            hue       : hue        != nil ? hue!        : hueValue       (of:light),
            saturation: saturation != nil ? saturation! : saturationValue(of:light),
            brightness: brightness != nil ? brightness! : brightnessValue(of:light),
            opacity   : opacity
        )
    }
    private func ctColor(ct: Double? = nil, brightness:Double? = nil, opacity: Double = 1.0) -> Color {
        Color(UIColor(kelvin: 1_000_000.0 / (ct != nil ? ct! : Double(temperatureValue(of:light)))))
            .opacity(opacity)
    }
}
//MARK: - Misc
fileprivate struct SliderButtonConf {
    let action: () -> ()
    let image: ButtonImage
    let color: Color
    let accessibility:String
    enum ButtonImage {
        case system(String)
    }
}
fileprivate enum RowPositon { case first, last, inbetween }
fileprivate let material:Material = .thickMaterial
fileprivate func imageFrom(img:Img) -> Image { switch img { case .src(.system(let name)): return .init(systemName:name) } }
