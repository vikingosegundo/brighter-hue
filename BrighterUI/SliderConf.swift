//
//  SliderConf.swift
//  BrighterUI
//
//  Created by vikingosegundo on 05/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

struct SliderConf {
    let action: () -> ()
    let accessibility:String
}
