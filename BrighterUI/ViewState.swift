//
//  ViewState.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import BrighterModel

public
final class ViewState: ObservableObject  {
    @Published public var lights    : [Light   ] = []
    @Published public var rooms     : [Room    ] = []
    @Published public var favorites : [Favorite] = []
    @Published public var themeColor: Color      = .black
    public init(store:Store<AppState,AppState.Change>) {
        store.updated { self.process(state(in:store)) }
        process(store.state())
    }
    
    public func handle(msg: Message) {  }
    
    private func process(_ appState: AppState) {
        DispatchQueue.main.async {
            self.lights    = appState.lights.sorted(on:\.name, by:<)
            self.rooms     = appState.rooms .sorted(on:\.title,by:<)
            self.favorites = appState.favorites
        }
    }
}
