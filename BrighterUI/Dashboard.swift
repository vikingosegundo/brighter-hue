//
//  Dashboard.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 18.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import BrighterModel

struct Dashboard: View {
    init(_ rootHandler: @escaping (Message) -> ()) {
        self.rootHandler = rootHandler
    }
    var body: some View {
        ZStack {
            VStack {
                NavigationView {
                    ScrollView {
                        LazyVGrid(columns: columns) {
                            ForEach(Array(viewState.favorites.enumerated()), id:\.offset) { _, fav in
                                OnOffSwitchView(light: viewState.lights.first(where: { $0.id == fav.lightID })!, rootHandler:rootHandler)
                            }.padding()
                        }
                        .buttonStyle(PlainButtonStyle())
                        .navigationTitle("Dashboard")
                        .padding()
                        Spacer()
                    }
                }
            }
        }
    }
    @EnvironmentObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
    private let columns = [GridItem(.adaptive(minimum: 90, maximum: 100))]
}
