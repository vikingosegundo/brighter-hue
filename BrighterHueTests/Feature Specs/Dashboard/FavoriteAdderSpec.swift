//
//  FavoriteAdderSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 17/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble

import BrighterModel
@testable import BrighterHue
final
class FavoriteAdderSpec: QuickSpec {
    override func spec() {
        var store    : AppStore!                ; beforeEach { store = createDiskStore(pathInDocs: "dimmer.spec.json")                                         }; afterEach { destroy(&store) }
        var light    : Light!                   ; beforeEach { light = newLight()                                                                              }; afterEach { light  = nil    }
        var responses: [FavoriteAdder.Response]!; beforeEach { responses = []                                                                                  }; afterEach { responses = nil }
        var favorite: Favorite!                 ; beforeEach { favorite = Favorite(kind: .switchOnOff, lightID: light.id)                                      }; afterEach { favorite = nil  }
        var favadder : FavoriteAdder!           ; beforeEach { favadder = FavoriteAdder(store: store, responder: { responses.append($0)}) }; afterEach { favadder = nil  }
        
        describe("FavoriteAdder") {
            context("adding favorite") { beforeEach { request(favadder, to: .add(favorite)) }
                it("succeeds") { expect(responses) == [.favorite(.was(.added(favorite)))] } }
            context("removing favorite") { beforeEach { request(favadder, to: .remove(favorite)) }
                it("succeeds") { expect(responses) == [.favorite(.was(.removed(favorite)))] } }
        }
    }
}

extension FavoriteAdder.Response:Equatable {
    public static func == (lhs: FavoriteAdder.Response, rhs: FavoriteAdder.Response) -> Bool {
        switch (lhs,rhs) {
        case let (.favorite(.was(.added  (lfav))),.favorite(.was(.added  (rfav)))): return lfav == rfav
        case let (.favorite(.was(.removed(lfav))),.favorite(.was(.removed(rfav)))): return lfav == rfav
        default: return false
        }
    }
}
