//
//  DashboardFeatureSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 02/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
@testable import BrighterHue
import BrighterModel

let crateDashboard = createDashboardFeature

final
class DashboardFeatureSpec: QuickSpec {
    override func spec() {
        var store  : AppStore!         ; beforeEach { store = createDiskStore(pathInDocs: "DashboardFeatureSpec.json")  }; afterEach { destroy(&store) }
        var light  : Light!            ; beforeEach { light = newLight()                                                }; afterEach { light   = nil   }
        var fav    : Favorite!         ; beforeEach { fav = Favorite(kind: .switchOnOff, lightID:newLight().id)         }; afterEach { fav     = nil   }
        var msgs   : [Message]!        ; beforeEach { store.change(.by(.adding(.light(light))))                         }; afterEach { msgs    = nil   }
        var feature: ((Message) -> ())!; beforeEach { feature = crateDashboard(store){ msgs = (msgs ?? []) + [$0]}      }; afterEach { feature = nil   }
#if targetEnvironment(simulator)
        describe("Documentation") { 
            context("of DashboardFeature") { let docs = DashboardFeature.Documentation()
                it("describes 2 commands") { expect(docs.count).to(equal(2))            } } }
#endif
        describe("DashboardFeature"){
            context("without calling the feature"                 ) {
                it("it will contain no favorites"                 ) { expect(store.state().favorites) == [] }        }
            context("add a favorite"                              ) { beforeEach { feature(.dashboard(.add(fav)))    }
                it("it will contain that favorite"                ) { expect(store.state().favorites) == [ fav ]     }
                context("and remove it again"                     ) { beforeEach { feature(.dashboard(.remove(fav))) }
                    it("it will not contain that favorite anymore") { expect(store.state().favorites) == [ ]         } } } }
    }
}
