//
//  LightChangerSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 17/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble

import BrighterModel
@testable import BrighterHue

final
class LightChangerSpec: QuickSpec {
    override func spec() {
        var client   : MockClient!             ; beforeEach { client = MockClient(with: thelights, rooms: rooms)                                            }; afterEach { client = nil    }
        var store    : AppStore!               ; beforeEach { store = createDiskStore(pathInDocs: "dimmer.spec.json")                                       }; afterEach { destroy(&store) }
        var lstack   : LightsStack!            ; beforeEach { lstack = LightsStack(client: client)                                                          }; afterEach { lstack = nil    }
        var light    : Light!                  ; beforeEach { light = newLight()                                                                            }; afterEach { light  = nil    }
        var responses: [LightChanger.Response]!; beforeEach { responses = []                                                                                }; afterEach { responses = nil }
        var changer  : LightChanger!           ; beforeEach { changer = LightChanger(lightsStack: lstack, store: store, responder: { responses.append($0)}) }; afterEach { changer = nil   }
        describe("LightChanger") {
            context("changing") {
                context("title")     { beforeEach { request(changer,to: .change(.name("renamed"), on:light))                                                                }
                    it("succeeds")   { expect(responses) == [LightChanger.Response.changing(.title("renamed"), on:light.alter(by: .renaming(.it(to: "renamed"))), .succeeded)]  } }
                context("title")     { beforeEach { client.shouldFail = true; request(changer,to: .change(.name("renamed"), on:light))                                      }
                    it("fails")      { expect(responses) == [LightChanger.Response.changing(.title("renamed"), on: light, .failed(LightError.lightsEndpoint(nil)))]         } }
                context("interface") { beforeEach { request(changer,to: .change(.display(.scrubbing), on:light))                                                            }
                    it("succeeds")   { expect(responses) == [LightChanger.Response.changing(.display(.scrubbing), on: light, .succeeded)]                                   } }
                context("interface") { beforeEach { client.shouldFail = true; request(changer,to: .change(.display(.scrubbing), on:light))                                  }   /* there is no conceivable path how this can fail */
                    it("cant fail")  { expect(responses) == [LightChanger.Response.changing(.display(.scrubbing), on: light, .succeeded)]                                   } }
            }
        }
    }
}

extension LightChanger.Response: Equatable {
    public static func == (lhs: LightChanger.Response, rhs: LightChanger.Response) -> Bool {
        switch (lhs, rhs) {
        case let (.changing(.title(lt),           on: ll, .succeeded),        .changing(.title(rt),           on: rl, .succeeded        )): return lt == rt && ll.id == rl.id
        case let (.changing(.title(lt),           on: ll, .failed(.none)),    .changing(.title(rt),           on: rl, .failed(.none)    )): return lt == rt && ll.id == rl.id
        case let (.changing(.title(lt),           on: ll, .failed(.some(le))),.changing(.title(rt),           on: rl, .failed(.some(re)))): return lt == rt && ll.id == rl.id && le.localizedDescription == re.localizedDescription
        case let (.changing(.display(.scrubbing), on: ll, .succeeded),        .changing(.display(.scrubbing), on: rl, .succeeded        )): return ll.id == rl.id
        case let (.changing(.display(.scrubbing), on: ll, .failed(.none)),    .changing(.display(.scrubbing), on: rl, .failed(.none)    )): return ll.id == rl.id
        case let (.changing(.display(.scrubbing), on: ll, .failed(.some(le))),.changing(.display(.scrubbing), on: rl, .failed(.some(re)))): return ll.id == rl.id && le.localizedDescription == re.localizedDescription
        default: return false
        }
    }
}
