//
//  LightingFeatureSpec.swift
//  BrighterHueTests
//
//  Created by Manuel Meyer on 29.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
import BrighterModel
import BrighterTools
@testable import BrighterHue
import Foundation

func newLight() -> Light {
    Light(id:"01", name:"01").alter(
        by:
            .renaming(.it(to:"Turingzaal 1")),
            .setting (.brightness (to:0.5   )),
            .setting (.hue        (to:0.5   )),
            .setting (.saturation (to:0.5   )),
            .setting (.temperature(to:200.mk)),
            .turning (.it         (   .on   ))
    )
}

func firstLightIn(_ store:AppStore) -> Light { first(of:lights(in:state(in:store)))! }

let crateLightFeature = createLightingFeature

final
class LightingFeatureSpec: QuickSpec {
    override func spec() {
        var client : MockClient!       ; beforeEach { client = MockClient(with: thelights, rooms: rooms)                      }; afterEach { client = nil    }
        var store  : AppStore!         ; beforeEach { store = createDiskStore(pathInDocs: "dimmer.spec.json")                 }; afterEach { destroy(&store) }
        var lstack : LightsStack!      ; beforeEach { lstack = LightsStack(client: client)                                    }; afterEach { lstack = nil    }
        var light  : Light!            ; beforeEach { light = newLight()                                                      }; afterEach { light   = nil   }
        var msgs   : [Message]!        ; beforeEach { store.change(.by(.adding(.light(light))))                               }; afterEach { msgs    = nil   }
        var feature: ((Message) -> ())!; beforeEach { feature = crateLightFeature(store, lstack){ msgs = (msgs ?? []) + [$0]} }; afterEach { feature = nil   }
#if targetEnvironment(simulator)
        describe("Documentation") { // Uses Case Requests
            context("of Dimmer"                ) { let docs = Dimmer          .Documentation(); it("describes 8 requests + responses" ) { expect(docs.count).to(equal( 8)) } }
            context("of LightSwitcher"         ) { let docs = LightSwitcher   .Documentation(); it("describes 2 requests + responses" ) { expect(docs.count).to(equal( 2)) } }
            context("of LightChanger"          ) { let docs = LightChanger    .Documentation(); it("describes 3 requests + responses" ) { expect(docs.count).to(equal( 3)) } }
            context("of LightValueSetter"      ) { let docs = LightValueSetter.Documentation(); it("describes 3 requests + responses" ) { expect(docs.count).to(equal( 3)) } }
            context("of LightsLoader"          ) { let docs = LightsLoader    .Documentation(); it("describes 2 requests + responses" ) { expect(docs.count).to(equal( 2)) } }
//                                     Public Messages                                                                                                        Σ --
            context("of LightingFeature"       ) { let docs = LightingFeature .Documentation(); it("describes 18 commands"            ) { expect(docs.count).to(equal(18)) } }
            context("of all uses cases"        ) { let    countOfAllUseCases = Dimmer.Documentation().count + LightSwitcher.Documentation().count + LightChanger.Documentation().count + LightValueSetter.Documentation().count + LightsLoader.Documentation().count
                it("should have the same size of \(countOfAllUseCases) " // 18
                 + "as the LightingFeature doc")  { expect( countOfAllUseCases ).to(equal(LightingFeature.Documentation().count)) } }}
#endif
        beforeEach { feature(.lighting(.load(.lights))) }
        describe("ligthing feature") {
            context("loading") {
                context("lights") {
                    it("loads the lights into store's state") { expect     ( store.state().lights).to(haveCount(3)) } }
                context("rooms"                             ) { beforeEach { feature(.lighting(.load(.rooms)))      }
                    it("loads the rooms into store's state" ) { expect     ( store.state().rooms ).to(haveCount(4)) } } }
            context("switching" ) {
                context("first light in state") {
                    context("off"                                     ) { beforeEach { feature( .lighting( .turn(firstLightIn(store), .off))) }
                        it("will keed the light at the same index"    ) { expect     ( idValue(of:firstLightIn(store))).to(equal("01"))       }
                        it("will turn off the light"                  ) { expect     ( firstLightIn(store).isOn).to(equal(.off))              }
                        context("and on again"                        ) { beforeEach { feature( .lighting( .turn(firstLightIn(store), .on)))  }
                            it("will keep the light at the same index") { expect     ( firstLightIn(store).id).to(equal("01"))                }
                            it("will turn on the light"               ) { expect     ( firstLightIn(store).isOn).to(equal(.on))               }
                            it("won't change selected mode value"     ) { expect     ( modeValue(of:firstLightIn(store))).to(equal(.ct))      } } } } }
            context("dimming") {
                context("brightness by") {
                    context("increasing"                        ) { beforeEach { feature(.lighting(.increase(.brightness,by:10.pt,on:light))) }
                        context("by 10 points") {
                            it("light's bri will be 0.6"        ) { expect     ( brightnessValue(of:firstLightIn(store))).to(equal(0.6)) }
                            it("won't change selected mode"     ) { expect     ( firstLightIn(store).selectedMode).to(equal(.ct))        }
                            context("fails") {
                                beforeEach { client.shouldFail = true}
                                beforeEach { feature(.lighting(.increase(.brightness,by:10.pt,on:light))) }
                                it("light's bri will not be changed") { expect     ( brightnessValue(of:firstLightIn(store))).toEventually(equal(0.6)) } } }
                        context("beyound") {
                            context("upper bound"               ) { beforeEach { feature(.lighting(.increase(.brightness,by:50.pt,on:light)))              }
                                it("light's bri caps off at 1.0") { expect     ( brightnessValue(of:firstLightIn(store))).to(beCloseTo(1.0, within: 0.02)) } } } }
                    context("decreasing"                        ) { beforeEach { feature(.lighting(.decrease(.brightness,by:.pt(10),on:light)))            }
                        it("by 10 points"                       ) { expect     ( brightnessValue(of:firstLightIn(store))).to(equal(0.4))                   }
                        context("beyound") {
                            context("lower bound"               ) { beforeEach { feature(.lighting(.decrease(.brightness,by:.pt(70),on:light)))          }
                                it("light's bri caps off at 0.0") { expect     ( brightnessValue(of:firstLightIn(store))).to(beCloseTo(0, within: 0.02)) } } } } }
                context("hue by") {
                    context("increasing"                        ) { beforeEach { feature(.lighting(.increase(.hue,by:.pt(10),on:light))) }
                        it("by 10 points"                       ) { expect     ( hueValue(of:firstLightIn(store) )).to(equal(0.6))       }
                        it("will change selected mode to hsb"   ) { expect     ( firstLightIn(store).selectedMode).to(equal(.hsb))       }
                        context("fails") {
                            beforeEach { client.shouldFail = true}
                            beforeEach { feature(.lighting(.increase(.hue,by:10.pt,on:light))) }
                            it("light's bri will not be changed") { expect     ( hueValue(of:firstLightIn(store))).toEventually(equal(0.6)) } }
                        context("beyound") {
                            context("upper bound"               ) { beforeEach { feature(.lighting(.increase(.hue,by:.pt(50),on:light)))            }
                                it("light's hue caps off at 1.0") { expect     ( hueValue(of:firstLightIn(store))).to(beCloseTo(1.0, within: 0.02)) } } } }
                    context("decreasing"                        ) { beforeEach { feature(.lighting(.decrease(.hue,by:.pt(10),on:light)))            }
                        it("by 10 points"                       ) { expect     ( hueValue(of:firstLightIn(store))).to(equal(0.4))                   }
                        context("fails") {
                            beforeEach { client.shouldFail = true}
                            beforeEach { feature(.lighting(.decrease(.hue,by:10.pt,on:light))) }
                            it("light's bri will not be changed") { expect     ( hueValue(of:firstLightIn(store))).to(equal(0.4)) } }
                        context("beyound") {
                            context("lower bound"               ) { beforeEach { feature(.lighting(.decrease(.hue,by:.pt(70),on:light)))         }
                                it("light's hue caps off at 0.0") { expect     ( hueValue(of:firstLightIn(store))).to(beCloseTo(0, within:0.02)) } } } } }
                context("saturation by") {
                    context("increasing"                        ) { beforeEach { feature(.lighting(.increase(.saturation,by:.pt(10),on:light))) }
                        it("by 10 points"                       ) { expect     ( saturationValue(of:firstLightIn(store))).to(equal(0.6))        }
                        it("will change selected mode to hsb"   ) { expect     ( firstLightIn(store).selectedMode).to(equal(.hsb))              }
                        context("beyound") {
                            context("upper bound"               ) { beforeEach { feature(.lighting(.increase(.saturation,by:.pt(50),on:light)))           }
                                it("light's sat caps off at 1.0") { expect     ( saturationValue(of:firstLightIn(store))).to(beCloseTo(1.0, within:0.02)) } } } }
                    context("decreasing"                        ) { beforeEach { feature(.lighting(.decrease(.saturation,by:.pt(10),on:light)))           }
                        it("by 10 points"                       ) { expect     ( saturationValue(of:firstLightIn(store))).to(equal(0.4))                  }
                        it("will change selected mode to hsb"   ) { expect     ( firstLightIn(store).selectedMode).to(equal(.hsb))                        }
                        context("beyound") {
                            context("lower bound"               ) { beforeEach { feature(.lighting(.decrease(.saturation,by:.pt(70),on:light)))           }
                                it("light's sat caps off at 0.0") { expect     ( saturationValue(of:firstLightIn(store))).to(beCloseTo(0.0, within:0.02)) } } } } }
                context("ct by") {
                    context("increasing"                        ) { beforeEach { feature(.lighting(.increase(.colortemp,by:.pt(10),on:light)))  }
                        it("by 10 points"                       ) { expect     ( temperatureValue(of:firstLightIn(store))).to(equal(234))       }
                        it("will change selected mode to ct"    ) { expect     ( firstLightIn(store).selectedMode).to(equal(.ct))               }
                        context("beyound") {
                            context("upper bound"               ) { beforeEach { feature(.lighting(.increase(.colortemp,by:.pt(100),on:light))) }
                                it("caps off at 500"            ) { expect     ( temperatureValue(of:firstLightIn(store))).to(equal(500))       } } } }
                    context("decreasing"                        ) { beforeEach { feature(.lighting(.decrease(.colortemp,by:.pt(10),on:light)))  }
                        it("by 10 points"                       ) { expect     ( temperatureValue(of:firstLightIn(store))).to(equal(166))       }
                        it("will change selected mode to ct"    ) { expect     ( firstLightIn(store).selectedMode).to(equal(.ct))               }
                        context("beyound") {
                            context("lower bound"               ) { beforeEach { feature(.lighting(.decrease(.colortemp,by:.pt(100),on:light))) }
                                it("caps off at 153"            ) { expect     ( temperatureValue(of:firstLightIn(store))).to(equal(153))       } } } } } }
            context("change") {
                context("light's name"                                     ) { beforeEach { feature( .lighting(.change(.name("new name"), on: light))           ) }
                    it("renames store's first light names"                 ) { expect     ( first(of:lights(in:state(in:store)))?.name).to(equal("new name")    ) }
                    it("wont change selected mode"                         ) { expect     ( firstLightIn(store).selectedMode).to(equal(.ct))    } }
                context("light's interface display"                        ) { beforeEach { feature( .lighting(.change(.display(.scrubbing), on: light))        ) }
                    it("will set display to scrubbing"                     ) { expect     ( first(of:lights(in:state(in:store)))?.display).to(equal(.scrubbing) ) }
                    it("wont change selected mode"                         ) { expect     ( firstLightIn(store).selectedMode).to(equal(.ct))    }
                    context("changing it back"                             ) { beforeEach { feature( .lighting(.change(.display(.slider), on: light))           ) }
                        it("switches first light's display to slider"      ) { expect     ( first(of:lights(in:state(in:store)))?.display).to(equal(.slider)    ) } } } }
            context("apply") {
                context("new values") {
                    context("for brigntess on first light by commando"     ) { beforeEach { feature(.lighting(.apply(.values(.bri(0.1)),on:firstLightIn(store))))             }
                        it("changes the lamps brightness in state"         ) { expect     ( brightnessValue(of:firstLightIn(store))).to(equal(0.1))                           }
                        it("wont change selected mode"                     ) { expect     ( firstLightIn(store).selectedMode).to(equal(.ct))                                  } }
                    context("for ct on first light by commando"            ) { beforeEach { feature(.lighting(.apply(.values(.ct(190, 0.2)),on:firstLightIn(store))))         }
                        it("changes the lights brightness in state"        ) { expect     ( brightnessValue(of:firstLightIn(store))).to(equal(0.2))                           }
                        it("changes the lights color temperatuire in state") { expect     ( temperatureValue(of:firstLightIn(store))).to(equal(190))                          }
                        it("wont change selected mode"                     ) { expect     ( firstLightIn(store).selectedMode).to(equal(.ct))                                  } }
                    context("for hsb color on first light by commando"     ) { beforeEach { feature(.lighting(.apply(.values(.hsb(0.25, 0.5, 0.75)),on:firstLightIn(store)))) }
                        it("changes the lights hue in state"               ) { expect     ( hueValue(of:firstLightIn(store))).to(equal(0.25))                                 }
                        it("changes the lights saturatiom in state"        ) { expect     ( saturationValue(of:firstLightIn(store))).to(equal(0.5))                           }
                        it("changes the lights brightness in state"        ) { expect     ( brightnessValue(of:firstLightIn(store))).to(equal(0.75))                          }
                        it("will change selected mode to hsb"              ) { expect     ( firstLightIn(store).selectedMode).to(equal(.hsb))                                 } } }
                context("overshooting values") {
                    context("for hsb color on first light by commando"     ) { beforeEach { feature(.lighting(.apply(.values(.hsb(1.25, 1.5, 1.75)),on:firstLightIn(store)))) }
                        it("changes the lights hue in state"               ) { expect     ( hueValue(of:firstLightIn(store))).to(equal(1.0))                                  }
                        it("caps the lights saturation in state to 1.0"    ) { expect     ( saturationValue(of:firstLightIn(store))).to(equal(1.0))                           }
                        it("caps the lights brightness in state to 1.0"    ) { expect     ( brightnessValue(of:firstLightIn(store))).to(equal(1.0))                           }
                        it("will change selected mode to hsb"              ) { expect     ( firstLightIn(store).selectedMode).to(equal(.hsb)) }}}
                context("undershooting values") {
                    context("for hsb color on first light by commando"     ) { beforeEach { feature(.lighting(.apply(.values(.hsb(-0.25, -0.5, -0.75)),on:firstLightIn(store)))) }
                        it("caps the lights hue in state to 0.0"           ) { expect     ( hueValue(of:firstLightIn(store))).to(equal(0.0))                                     }
                        it("caps the lights saturatiom in state to 0.0"    ) { expect     ( saturationValue(of:firstLightIn(store))).to(equal(0.0))                              }
                        it("caps the lights brightness in state to 0.0"    ) { expect     ( brightnessValue(of:firstLightIn(store))).to(equal(0.0))                              }
                        it("will change selected mode to hsb"              ) { expect     ( firstLightIn(store).selectedMode).to(equal(.hsb))                                    } } }
            }
        }
    }
}
