//
//  LightValueSetterSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 17/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
import BrighterModel

@testable import BrighterHue

final
class LightValueSetterSpec: QuickSpec {
    override func spec() {
        var client   : MockClient!                 ; beforeEach { client = MockClient(with: thelights, rooms: rooms)                                               }; afterEach { client = nil    }
        var store    : AppStore!                   ; beforeEach { store = createDiskStore(pathInDocs: "dimmer.spec.json")                                          }; afterEach { destroy(&store) }
        var lstack   : LightsStack!                ; beforeEach { lstack = LightsStack(client: client)                                                             }; afterEach { lstack = nil    }
        var light    : Light!                      ; beforeEach { light = newLight()                                                                               }; afterEach { light  = nil    }
        var responses: [LightValueSetter.Response]!; beforeEach { responses = []                                                                                   }; afterEach { responses = nil }
        var setter   : LightValueSetter!           ; beforeEach { setter = LightValueSetter(lightsStack: lstack, store: store, responder: { responses.append($0)}) }; afterEach { setter = nil    }
        describe("LightValueSetter") {
            context("setting color temperature") { beforeEach {setter.request(.apply(.values(.ct(220, 0.5)), on: light))                              }
                it("succeeds"                  ) { expect(responses).to(equal([.applying(.values(.ct(220, 0.5)), on: light, .succeeded)]))                  } }
            context("setting color temperature") { beforeEach {client.shouldFail = true; setter.request( .apply(.values(.ct(220, 0.5)), on: light))
            }
                it("fails"                     ) { expect(responses).to(equal([.applying(.values(.ct(220, 0.5)), on: light, .failed(LightError.unknown))])) } }
            context("setting brightness"       ) { beforeEach { request(setter, to: .apply(.values(.bri(0.75)), on: light))                                 }
                it("succeeds"                  ) { expect(responses).to(equal([.applying(.values(.bri(0.75)), on: light, .succeeded)]))                     } }
            context("setting brightness"       ) { beforeEach {client.shouldFail = true; request(setter, to: .apply(.values(.bri(0.75)), on: light))        }
                it("fails"                     ) { expect(responses).to(equal([.applying(.values(.bri(0.75)), on: light, .failed(LightError.unknown))]))    } }
            context("setting hsb"              ) { beforeEach { request(setter, to: .apply(.values(.hsb(1, 1, 1)), on: light))                              }
                it("succeeds"                  ) { expect(responses).to(equal([.applying(.values(.hsb(1, 1, 1)), on: light, .succeeded)]))                  } }
            context("setting hsb"              ) { beforeEach {client.shouldFail = true; request(setter, to: .apply(.values(.hsb(1, 1, 1)), on: light))     }
                it("fails"                     ) { expect(responses).to(equal([.applying(.values(.hsb(1, 1, 1)), on: light, .failed(LightError.unknown))])) } }
        }
    }
}
