//
//  DimmerSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 17/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
import BrighterModel

@testable import BrighterHue

final
class DimmerSpec: QuickSpec {
    override func spec() {
        var client   : MockClient!       ; beforeEach { client = MockClient(with: thelights, rooms: rooms)                                      }; afterEach { client = nil    }
        var store    : AppStore!         ; beforeEach { store = createDiskStore(pathInDocs: "dimmer.spec.json")                                 }; afterEach { destroy(&store) }
        var lstack   : LightsStack!      ; beforeEach { lstack = LightsStack(client: client)                                                    }; afterEach { lstack = nil    }
        var light    : Light!            ; beforeEach { light = newLight()                                                                      }; afterEach { light  = nil    }
        var responses: [Dimmer.Response]!; beforeEach { responses = []                                                                          }; afterEach { responses = nil }
        var dimmer   : Dimmer!           ; beforeEach { dimmer = Dimmer(lightsStack: lstack, store: store, responder: { responses.append($0) }) }; afterEach { dimmer = nil    }
        describe("Dimming") {
            context("brightness") {
                context("by increasing by 10pt") { beforeEach { request(dimmer, to: .increase(.brightness, by: .pt(10), on: light))                                               }
                    it("successfully"          ) { expect(responses).to(equal([.increase(.brightness, by:.pt(10), on: light.alter(by: .setting(.brightness(to:0.6))), .succeeded)]))  } }
                context("by increasing by 10pt") { beforeEach { client.shouldFail = true; request(dimmer, to: .increase(.brightness, by: .pt(10), on: light))                     }
                    it("fails"                 ) { expect(responses).to(equal([.increase(.brightness, by:.pt(10), on: light, .failed(LightError.unknown))]))                      } }
                context("by decrease by 10pt"  ) { beforeEach { request(dimmer, to: .decrease(.brightness, by: .pt(10), on: light))                                               }
                    it("successfully"          ) { expect(responses).to(equal([.decrease(.brightness, by:.pt(10), on: light.alter(by: .setting(.brightness(to:0.4))), .succeeded)]))  } }
                context("by decrease by 10pt"  ) { beforeEach { client.shouldFail = true; request(dimmer, to: .decrease(.brightness, by: .pt(10), on: light))                     }
                    it("fails"                 ) { expect(responses).to(equal([.decrease(.brightness, by:.pt(10), on: light, .failed(LightError.unknown))]))                      } }
            }
            context("hue") {
                context("by increasing by 10pt") { beforeEach { request(dimmer, to: .increase(.hue, by: .pt(10), on: light))                                              }
                    it("successfully"          ) { expect(responses).to(equal([.increase(.hue, by:.pt(10), on: light.alter(by: .setting(.brightness(to:0.6))), .succeeded)])) } }
                context("by increasing by 10pt") { beforeEach { client.shouldFail = true; request(dimmer, to: .increase(.hue, by: .pt(10), on: light))                    }
                    it("fails"                 ) { expect(responses).to(equal([.increase(.hue, by:.pt(10), on: light, .failed(LightError.unknown))]))                     } }
                context("by decreasing by 10pt") { beforeEach { request(dimmer, to: .decrease(.hue, by: .pt(10), on: light))                                              }
                    it("successfully"          ) { expect(responses).to(equal([.decrease(.hue, by:.pt(10), on: light.alter(by: .setting(.hue(to:0.4))), .succeeded)]))        } }
                context("by decreasing by 10pt") { beforeEach { client.shouldFail = true; request(dimmer, to: .decrease(.hue, by: .pt(10), on: light))                    }
                    it("fails"                 ) { expect(responses).to(equal([.decrease(.hue, by:.pt(10), on: light, .failed(LightError.unknown))]))                     } }
            }
            context("saturation") {
                context("by increasing by 10pt") { beforeEach { request(dimmer, to: .increase(.saturation, by: .pt(10), on: light))                                              }
                    it("successfully"          ) { expect(responses).to(equal([.increase(.saturation, by:.pt(10), on: light.alter(by: .setting(.brightness(to:0.6))), .succeeded)])) } }
                context("by increasing by 10pt") { beforeEach { client.shouldFail = true; request(dimmer, to: .increase(.saturation, by: .pt(10), on: light))                    }
                    it("fails"                 ) { expect(responses).to(equal([.increase(.saturation, by:.pt(10), on: light, .failed(LightError.unknown))]))                     } }
                context("by decreasing by 10pt") { beforeEach { request(dimmer, to: .decrease(.saturation, by: .pt(10), on: light))                                              }
                    it("successfully"          ) { expect(responses).to(equal([.decrease(.saturation, by:.pt(10), on: light.alter(by: .setting(.brightness(to:0.6))), .succeeded)])) } }
                context("by decreasing by 10pt") { beforeEach { client.shouldFail = true; request(dimmer, to: .decrease(.saturation, by: .pt(10), on: light))                    }
                    it("fails"                 ) { expect(responses).to(equal([.decrease(.saturation, by:.pt(10), on: light, .failed(LightError.unknown))]))                     } }
            }
            context("colortemp") {
                context("by increasing by 10pt") { beforeEach { request(dimmer, to: .increase(.colortemp, by: .pt(10), on: light))                                                       }
                    it("successfully"          ) { expect(responses).to(equal([.increase(.colortemp, by:.pt(10), on:light.alter(by: .setting(.temperature(to: .mirek(234)))), .succeeded)])) } }
                context("by increasing by 10pt") { beforeEach { client.shouldFail = true; request(dimmer, to: .increase(.colortemp, by: .pt(10), on: light))                             }
                    it("fails"                 ) { expect(responses).to(equal([.increase(.colortemp, by:.pt(10), on: light, .failed(LightError.unknown))]))                              } }
                context("by decreasing by 10pt") { beforeEach { request(dimmer, to: .decrease(.colortemp, by: .pt(10), on: light))                                                       }
                    it("successfully"          ) { expect(responses).to(equal([.decrease(.colortemp, by:.pt(10), on:light.alter(by: .setting(.temperature(to: .mirek(166)))), .succeeded)])) } }
                context("by decreasing by 10pt") { beforeEach { client.shouldFail = true; request(dimmer, to: .decrease(.colortemp, by: .pt(10), on: light))                             }
                    it("fails"                 ) { expect(responses).to(equal([.decrease(.colortemp, by:.pt(10), on: light, .failed(LightError.unknown))]))                              } }
            }
        }
    }
}

extension Dimmer.Response:Equatable {
    public static func == (lhs: Dimmer.Response, rhs: Dimmer.Response) -> Bool {
        switch (lhs, rhs) {
        case let (.increase(.brightness, by: .pt(lpt), on: llight, .succeeded),.increase(.brightness, by: .pt(rpt), on: rlight, .succeeded)) : return lpt == rpt && llight.id == rlight.id
        case let (.increase(.brightness, by: .pt(lpt), on: llight, .failed   ),.increase(.brightness, by: .pt(rpt), on: rlight, .failed   )) : return lpt == rpt && llight.id == rlight.id
        case let (.decrease(.brightness, by: .pt(lpt), on: llight, .succeeded),.decrease(.brightness, by: .pt(rpt), on: rlight, .succeeded)) : return lpt == rpt && llight.id == rlight.id
        case let (.decrease(.brightness, by: .pt(lpt), on: llight, .failed   ),.decrease(.brightness, by: .pt(rpt), on: rlight, .failed   )) : return lpt == rpt && llight.id == rlight.id
        case let (.increase(.hue,        by: .pt(lpt), on: llight, .succeeded),.increase(.hue,        by: .pt(rpt), on: rlight, .succeeded)) : return lpt == rpt && llight.id == rlight.id
        case let (.increase(.hue,        by: .pt(lpt), on: llight, .failed   ),.increase(.hue,        by: .pt(rpt), on: rlight, .failed   )) : return lpt == rpt && llight.id == rlight.id
        case let (.decrease(.hue,        by: .pt(lpt), on: llight, .succeeded),.decrease(.hue,        by: .pt(rpt), on: rlight, .succeeded)) : return lpt == rpt && llight.id == rlight.id
        case let (.decrease(.hue,        by: .pt(lpt), on: llight, .failed   ),.decrease(.hue,        by: .pt(rpt), on: rlight, .failed   )) : return lpt == rpt && llight.id == rlight.id
        case let (.increase(.saturation, by: .pt(lpt), on: llight, .succeeded),.increase(.saturation, by: .pt(rpt), on: rlight, .succeeded)) : return lpt == rpt && llight.id == rlight.id
        case let (.increase(.saturation, by: .pt(lpt), on: llight, .failed   ),.increase(.saturation, by: .pt(rpt), on: rlight, .failed   )) : return lpt == rpt && llight.id == rlight.id
        case let (.decrease(.saturation, by: .pt(lpt), on: llight, .succeeded),.decrease(.saturation, by: .pt(rpt), on: rlight, .succeeded)) : return lpt == rpt && llight.id == rlight.id
        case let (.decrease(.saturation, by: .pt(lpt), on: llight, .failed   ),.decrease(.saturation, by: .pt(rpt), on: rlight, .failed   )) : return lpt == rpt && llight.id == rlight.id
        case let (.increase(.colortemp,  by: .pt(lpt), on: llight, .succeeded),.increase(.colortemp,  by: .pt(rpt), on: rlight, .succeeded)) : return lpt == rpt && llight.id == rlight.id
        case let (.increase(.colortemp,  by: .pt(lpt), on: llight, .failed   ),.increase(.colortemp,  by: .pt(rpt), on: rlight, .failed   )) : return lpt == rpt && llight.id == rlight.id
        case let (.decrease(.colortemp,  by: .pt(lpt), on: llight, .succeeded),.decrease(.colortemp,  by: .pt(rpt), on: rlight, .succeeded)) : return lpt == rpt && llight.id == rlight.id
        case let (.decrease(.colortemp,  by: .pt(lpt), on: llight, .failed   ),.decrease(.colortemp,  by: .pt(rpt), on: rlight, .failed   )) : return lpt == rpt && llight.id == rlight.id
        default:
            return false
        }
    }
}
