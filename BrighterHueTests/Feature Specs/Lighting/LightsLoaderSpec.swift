//
//  LightsLoaderSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 17/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble

import BrighterModel
@testable import BrighterHue

final class LightsLoaderSpec: QuickSpec {
    override func spec() {
        var client   : MockClient!             ; beforeEach { client = MockClient(with: thelights, rooms: rooms)                                           }; afterEach { client = nil    }
        var store    : AppStore!               ; beforeEach { store = createDiskStore(pathInDocs: "dimmer.spec.json")                                      }; afterEach { destroy(&store) }
        var lstack   : LightsStack!            ; beforeEach { lstack = LightsStack(client: client)                                                         }; afterEach { lstack = nil    }
        var responses: [LightsLoader.Response]!; beforeEach { responses = []                                                                               }; afterEach { responses = nil }
        var loader   : LightsLoader!           ; beforeEach { loader = LightsLoader(lightsStack: lstack, store: store, responder: { responses.append($0)}) }; afterEach { loader = nil    }
        describe("LightsLoader") {
            context("loading"){
                context("all lights") { beforeEach { request(loader, to: .load(.all(.lights)))                                }
                    it("succeeds"   ) { expect(responses) == [.loading(.lights(thelights), .succeeded)]                       } }
                context("all lights") { beforeEach { client.shouldFail = true;request(loader, to: .load(.all(.lights)))       }
                    it("fails"      ) { expect(responses) == [.loading(.lights([]), .failed(LightError.lightsEndpoint(nil)))] } }
                context("all rooms" ) { beforeEach { request(loader, to: .load(.all(.rooms)))                                 }
                    it("succeeds"   ) { expect(responses) == [.loading(.rooms(rooms), .succeeded)]                            } }
                context("all rooms" ) { beforeEach { client.shouldFail = true;request(loader, to: .load(.all(.rooms)))        }
                    it("fails"      ) { expect(responses) == [.loading(.rooms([]), .failed(LightError.lightsEndpoint(nil)))]  } }
            }
        }
    }
}

extension LightsLoader.Response:Equatable {
    public static func == (lhs: LightsLoader.Response, rhs: LightsLoader.Response) -> Bool {
        switch (lhs, rhs) {
        case let (.loading(.lights(llights), .succeeded        ),.loading(.lights(rlights), .succeeded        )): return llights == rlights
        case let (.loading(.lights(llights), .failed(.none)    ),.loading(.lights(rlights), .failed(.none)    )): return llights == rlights
        case let (.loading(.lights(llights), .failed(.some(le))),.loading(.lights(rlights), .failed(.some(re)))): return llights == rlights && le.localizedDescription == re.localizedDescription
        case let (.loading(.rooms (lrooms),  .succeeded        ),.loading(.rooms (rrooms),  .succeeded        )): return lrooms == rrooms
        case let (.loading(.rooms (lrooms),  .failed(.none)    ),.loading(.rooms (rrooms),  .failed(.none)    )): return lrooms == rrooms
        case let (.loading(.rooms (lrooms),  .failed(.some(le))),.loading(.rooms (rrooms),  .failed(.some(re)))): return lrooms == rrooms && le.localizedDescription == re.localizedDescription
        default: return false
        }
    }
}
