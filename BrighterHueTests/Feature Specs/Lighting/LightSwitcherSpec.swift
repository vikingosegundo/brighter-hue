//
//  LightSwitcherSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 17/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble

import BrighterModel
@testable import BrighterHue

final class LightSwitcherSpec: QuickSpec {
    override func spec() {
        var client    : MockClient!             ; beforeEach { client = MockClient(with: thelights, rooms: rooms)                                              }; afterEach { client = nil    }
        var store    : AppStore!                ; beforeEach { store = createDiskStore(pathInDocs: "dimmer.spec.json")                                         }; afterEach { destroy(&store) }
        var lstack   : LightsStack!             ; beforeEach { lstack = LightsStack(client: client)                                                            }; afterEach { lstack = nil    }
        var light    : Light!                   ; beforeEach { light = newLight()                                                                              }; afterEach { light  = nil    }
        var responses: [LightSwitcher.Response]!; beforeEach { responses = []                                                                                  }; afterEach { responses = nil }
        var switcher : LightSwitcher!           ; beforeEach { switcher = LightSwitcher(lightsStack: lstack, store: store, responder: { responses.append($0)}) }; afterEach { switcher = nil  }
        describe("LightSwitcher") {
            context("switching") {
                context("light on")  { beforeEach { request(switcher, to: .turn(light, .on))                           }
                    it("succeeds")   { expect(responses) == [.light(light, .was(turned: .on))]                         } }
                context("light off") { beforeEach { request(switcher, to: .turn(light, .off))                          }
                    it("succeeds")   { expect(responses) == [.light(light, .was(turned: .off))]                        } }
                context("light off") { beforeEach { client.shouldFail = true;request(switcher, to: .turn(light, .on))  }
                    it("fails")      { expect(responses) == []                                                         } }
                context("light off") { beforeEach { client.shouldFail = true;request(switcher, to: .turn(light, .off)) }
                    it("succeeds")   { expect(responses) == []                                                         } }
            }
        }
    }
}

extension LightSwitcher.Response: Equatable {
    public static func == (lhs: LightSwitcher.Response, rhs: LightSwitcher.Response) -> Bool {
        switch (lhs, rhs) {
        case let (.light(ll, .was(turned: .on )),.light(rl,.was(turned:.on ))): return ll.id == rl.id
        case let (.light(ll, .was(turned: .off)),.light(rl,.was(turned:.off))): return ll.id == rl.id
        default: return false
        }
    }
}
