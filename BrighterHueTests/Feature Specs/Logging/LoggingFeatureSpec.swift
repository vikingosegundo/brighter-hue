//
//  LoggingFeatureSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 24/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//


import Quick
import Nimble
@testable import BrighterHue
import BrighterModel

final
class LoggingFeatureSpec: QuickSpec {
    override func spec() {
        var loggingFeature: Input!
        describe("LoggingFeature") {
            context("prints") {
                beforeEach { loggingFeature = createLoggingFeature() }
                beforeEach { loggingFeature(.logging(.write(.init(text: "never seen again")))) }
                it("successfully") { expect(loggingFeature).toNot(beNil()) }
            }
            context("access item") {
                var printedItem: LogItem?
                beforeEach { loggingFeature = createLoggingFeature(print: { printedItem = $0  }) }
                afterEach  { printedItem = nil }
                beforeEach { loggingFeature(.logging(.write(.init(text: "text")))) }
                it("text") { expect(printedItem?.text) == "text" }
            }
        }
    }
}
