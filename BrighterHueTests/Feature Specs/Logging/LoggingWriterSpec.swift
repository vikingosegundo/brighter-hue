//
//  LoggingWriterSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 24/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//


import Quick
import Nimble
@testable import BrighterHue
import BrighterModel

final
class LoggingWriterSpec: QuickSpec {
    
    override func spec() {
        var writer: LogitemWriter!
        describe("LogitemWriter") {
            var printedItem: LogItem?
            afterEach  { printedItem = nil }
            context("create default") {
                beforeEach { writer = LogitemWriter(); request(writer, to: .write(item: .init(text: "never seen again"))) }
                it("successfully") { expect(writer).toNot(beNil()) }

            }
            
            context("log item") {
                beforeEach { writer = LogitemWriter(print: { printedItem = $0 }) }
                beforeEach { request(writer, to: .write(item: .init(text: "Hello World"))) }
                it("will log item with text") { expect(printedItem?.text) == "Hello World" }
                it("will log item with date") { expect(printedItem?.date).to(beCloseTo(Date())) }
            }
        }
    }
}
