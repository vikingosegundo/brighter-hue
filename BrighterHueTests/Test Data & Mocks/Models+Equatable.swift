//
//  Models+Equatable.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 25/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

@testable import BrighterHue
import BrighterModel

extension Img:Equatable {
    public static func == (lhs: Img, rhs: Img) -> Bool {
        switch (lhs, rhs) {
        case let (.src(.system(lsrc)),.src(.system(rsrc))): return lsrc == rsrc
        }
    }
}
extension Img._Source:Equatable {
    public static func == (lhs: Img._Source, rhs: Img._Source) -> Bool {
        switch (lhs, rhs) {
        case let (.system(ln),.system(rn)): return ln == rn
        }
    }
}
extension Light.Values: Equatable {
    public static func == (l: Light.Values, r: Light.Values) -> Bool {
        switch (l,r) {
        case let (.bri(l         ), .bri(r)         ): return l == r
        case let (.hsb(lh, ls, lb), .hsb(rh, rs, rb)): return lh == rh && ls == rs && lb == rb
        case let (.ct(lt, lb     ), .ct (rt,     rb)): return lt == rt && lb == rb
        default : return false
        }
    }
}
extension Outcome: Equatable {
    public static func == (l: Outcome, r: Outcome) -> Bool {
        switch (l,r) {
        case     (.succeeded,       .succeeded       ): return true
        case     (.failed(.none),   .failed(.none)   ): return true
        case let (.failed(.some(l)),.failed(.some(r))): return l.localizedDescription == r.localizedDescription
        default: return false
        }
    }
}
extension LightValueSetter.Response: Equatable {
    public static func == (l: LightValueSetter.Response, r: LightValueSetter.Response) -> Bool {
        switch (l,r) {
        case let (.applying(.values(lvalues), on: llight, loutcome),.applying(.values(rvalues), on: rlight, routcome)): return llight.id == rlight.id && lvalues == rvalues && loutcome == routcome
        }
    }
}
extension Loaded:Equatable {
    public static func == (lhs: Loaded, rhs: Loaded) -> Bool {
        switch (lhs, rhs) {
        case let (.lights(llights),.lights(rlights)): return llights == rlights
        case let (.rooms (lrooms), .rooms (rrooms )): return lrooms  == rrooms
        default: return false
        }
    }
}
extension Apply:Equatable {
    public static func == (lhs: Apply, rhs: Apply) -> Bool {
        switch (lhs, rhs) {
        case let (.values(lvalues),.values(rvalues)): return lvalues == rvalues
        }
    }
}
extension Alter:Equatable {
    public static func == (lhs: Alter, rhs: Alter) -> Bool {
        switch (lhs, rhs) {
        case let (.name   (lname),.name   (rname)): return lname == rname
        case let (.display(ldisp),.display(rdisp)): return ldisp == rdisp
        default: return false
        }
    }
}
extension Message._Lighting: Equatable {
    public static func == (lhs: Message._Lighting, rhs: Message._Lighting) -> Bool {
        switch (lhs, rhs) {
        case let (.turn      (llight, lturn                         ),.turn      (rlight, rturn                         )): return llight == rlight && lturn == rturn
        case let (.turning   (llight, lturn,                loutcome),.turning   (rlight, rturn,                routcome)): return llight == rlight && lturn == rturn && loutcome == routcome
        case let (.apply     (lapply,           on: llight          ),.apply     (rapply,           on: rlight          )): return llight == rlight && lapply == rapply
        case let (.applying  (lapply,           on: llight, loutcome),.applying  (rapply,           on: rlight, routcome)): return llight == rlight && lapply == rapply && loutcome == routcome
        case let (.change    (lalter,           on: llight          ),.change    (ralter,           on: rlight          )): return llight == rlight && lalter == ralter
        case let (.changing  (lalter,           on: llight, loutcome),.changing  (ralter,           on: rlight, routcome)): return llight == rlight && lalter == ralter && loutcome == routcome
        case let (.increase  (lvalue, by: linc, on: llight          ),.increase  (rvalue, by: rinc, on: rlight          )): return llight == rlight && lvalue == rvalue && linc == rinc
        case let (.increasing(lvalue, by: linc, on: llight, loutcome),.increasing(rvalue, by: rinc, on: rlight, routcome)): return llight == rlight && lvalue == rvalue && linc == rinc && loutcome == routcome
        case let (.decrease  (lvalue, by: linc, on: llight          ),.decrease  (rvalue, by: rinc, on: rlight          )): return llight == rlight && lvalue == rvalue && linc == rinc
        case let (.decreasing(lvalue, by: linc, on: llight, loutcome),.decreasing(rvalue, by: rinc, on: rlight, routcome)): return llight == rlight && lvalue == rvalue && linc == rinc && loutcome == routcome
        default: return false
        }
    }
}
extension LogItem:Equatable {
    public static func == (lhs: LogItem, rhs: LogItem) -> Bool {
        lhs.text == rhs.text && lhs.date == rhs.date
    }
}
extension Message._Logging:Equatable {
    public static func == (lhs: Message._Logging, rhs: Message._Logging) -> Bool {
        switch (lhs, rhs) {
        case let (.write(litem),.write(ritem)): return litem == ritem
        }
    }
}
extension Message._Dashboard:Equatable {
    public static func == (lhs: Message._Dashboard, rhs: Message._Dashboard) -> Bool {
        switch (lhs, rhs) {
        case let (.add            (lfav         ),.add            (rfav          )): return lfav == rfav
        case let (.remove         (lfav         ),.remove         (rfav          )): return lfav == rfav
        case let (.favoriteAdded  (lfav, loutcom),.favoriteAdded  (rfav, routcome)): return lfav == rfav && loutcom == routcome
        case let (.favoriteRemoved(lfav, loutcom),.favoriteRemoved(rfav, routcome)): return lfav == rfav && loutcom == routcome
        default: return false
        }
    }
}
extension Message:Equatable {
    public static func == (lhs: Message, rhs: Message) -> Bool {
        switch (lhs, rhs) {
        case let (.lighting (ll),.lighting (rl)): return ll == rl
        case let (.dashboard(ld),.dashboard(rd)): return ld == rd
        case let (.logging  (ll),.logging  (rl)): return ll == rl
        default: return false
        }
    }
}
