//
//  Data.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 18/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel

let thelights = [
    alter(Light(id:"01", name:"Fish Tank"),
          by:
            .turning(         .it(.on      )),
            .setting(        .hue(to:0.15  )),
            .setting( .brightness(to:0.47  )),
            .setting( .saturation(to:0.65  )),
            .setting(.temperature(to:300.mk)),
             .adding(       .mode(.ct      )),
             .adding(       .mode(.hsb     ))
         )
    ,
    alter(Light(id:"02", name:"Lego Dungeon"),
          by:
            .turning(         .it(.on      )),
            .setting(        .hue(to:0.55  )),
            .setting( .brightness(to:0.7   )),
            .setting( .saturation(to:0.8   )),
            .setting(.temperature(to:300.mk)),
             .adding(       .mode(.ct      )),
             .adding(       .mode(.hsb     ))
         )
    ,
    alter(Light(id:"03", name:"Dog House"),
          by:
            .turning(         .it(.on      )),
            .setting(        .hue(to:0.3   )),
            .setting( .saturation(to:0.9   )),
            .setting( .brightness(to:0.5   )),
            .setting(.temperature(to:300.mk)),
             .adding(       .mode(.ct      )),
             .adding(       .mode(.hsb     ))
         )
]

let rooms = [
    Room(id:"a",title:"a"),
    Room(id:"b",title:"b"),
    Room(id:"c",title:"c"),
    Room(id:"d",title:"d"),
]
