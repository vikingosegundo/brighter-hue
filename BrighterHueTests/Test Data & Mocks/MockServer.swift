//
//  MockServer.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 21/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import   Swifter
import Foundation

final class MockServer {
    var server:HttpServer!
    init() {
        server = self.createServer()
    }
    var shouldFail = false
    func start() {
        try! server.start(8090, forceIPv4: true)
    }
    func stop() {
        server.stop()
    }
    private
    func createServer() -> HttpServer {
        let server = HttpServer()
        server["/api/uuu/lights/"        ] = { _ in self.shouldFail == false ? .ok(.json(self.loadFrom(jsonFile: "lights")))          : .badRequest(.none) }
        server["/api/uuu/groups/"        ] = { _ in self.shouldFail == false ? .ok(.json(self.loadFrom(jsonFile: "groups")))          : .badRequest(.none) }
        server["/api/uuu/lights/36/state"] = { _ in self.shouldFail == false ? .ok(.json([["success":["/lights/36/state/on":self.shouldFail]]])) : .badRequest(.none) }
        return server
    }
    private
    func loadFrom(jsonFile file:String) -> Any {
        let pathString = Bundle(for: type(of: self)).path(forResource: file, ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: pathString))
        return try! JSONSerialization.jsonObject(with: data, options: [])
    }
}
