//
//  StoreSpec.swift
//  StoreSpec
//
//  Created by vikingosegundo on 22/07/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
@testable import BrighterHue
import BrighterModel

final
class StoreSpec: QuickSpec {
    override func spec() {
        describe("Store") {
            var store: AppStore!
            beforeEach { store = createDiskStore(pathInDocs: "store.spec.json") }
             afterEach { destroy(&store) }
            context("creation") {
                it("a store is available"             ) { expect(store).toNot(beNil())}
                it("a store state's lights are empty" ) { expect(store.state().lights).to(equal([])) }
                it("a store state's rooms are empty"  ) { expect(store.state().rooms ).to(equal([])) } }
            context("reset") {
                beforeEach {
                    store.change(.by(.adding(.light(Light(id: "1", name: "1")))), .by(.replacing(.rooms(with: [Room(id: "r1", title: "r1")]))))
                }
                it("light was added before resetting" ) { expect(store.state().lights).to(equal([Light(id: "1", name: "1")]))}
                it("room was added before resetting"  ) { expect(store.state().rooms).to(equal([Room(id: "r1", title: "r1")]))}
                context("perform reset"               ) { beforeEach { reset(store) }
                    it("will delete lights from state") { expect(store.state().lights).to(equal([])) }
                    it("will delete rooms from state" ) { expect(store.state().rooms ).to(equal([])) } } }
        }
    }
}
