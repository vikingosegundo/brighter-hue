//
//  AppStateSpec.swift
//  BrighterHueTests
//
//  Created by Manuel Meyer on 10.07.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
@testable import BrighterHue
import BrighterModel
import Darwin

final
class AppStateSpec: QuickSpec {
    override func spec() {
        let appstate = AppState()
        let lights = (Light(id:"01",name:"light 1"), Light(id:"02",name:"light 2"))
        let rooms  = (Room( id:"01",title:"room 1"), Room( id:"02",title:"room 2"))
        let fav = Favorite(kind: .switchOnOff, lightID: "01")
        describe("newly created appstate") {
            it("doesnt have lights"            ) { expect(appstate.lights).to(haveCount(0)) }
            it("doesnt have rooms"             ) { expect(appstate.rooms ).to(haveCount(0)) } }
        describe("adding") {
            context("lights to appstate"       ) { let appstate = appstate.alter([.by(.adding(.light(lights.0))), .by(.adding(.light(lights.1)))])
                it("will contain 2 lights"     ) {     expect(appstate.lights).to(haveCount(2))                                                    } }
            context("rooms to appstate"        ) { let appstate = appstate.alter([.by(.replacing(.rooms(with: [rooms.0,rooms.1])))])
                it("will contain 2 rooms"      ) {     expect(appstate.rooms).to(haveCount(2))                                                     } }
            context("favorite to appstore"     ) { let appstate = appstate.alter([.by(.adding(.favorite(fav)))])
                it("will contain the favorite" ) {     expect(appstate.favorites) == [fav]                                                         } } }
        describe("replacing") {
            context("rooms" ) {
                context("by sending replcament command"     ) { let appstate = appstate.alter([.by(.replacing(.rooms(with: [Room(id:"03", title:"03")])))])
                    it("will replace the rooms in appstate" ) {     expect(appstate.rooms).to(equal([Room(id:"03", title:"03")]))                           } }}}
        describe("deleting") {
            context("rooms") {
                context("by replacing it with an empty list") { let appstate = appstate.alter([.by(.replacing(.rooms(with: [])))])
                    it("will replace the rooms in appstate" ) {     expect(appstate.rooms).to(equal([]))                           }}}
            context("favorite") {
                let appstate = appstate
                    .alter([.by(.adding(.favorite(fav)))])
                    .alter([.by(.removing(.favorite(fav)))])
                it("will remove favorite from appstate") {expect(appstate.favorites) == [] }
            }
        }
    }
}
