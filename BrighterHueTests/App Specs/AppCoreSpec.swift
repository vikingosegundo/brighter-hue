//
//  AppCoreSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 25/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
@testable import BrighterHue
import BrighterModel

final
class AppCoreSpec: QuickSpec {
    override func spec() {
        func appcore(with roothandler: @escaping (Message) -> ()) -> Input { createAppDomain(store: createDiskStore(pathInDocs: "appcorespec.json"), receivers: [], stackKind: .mock,rootHandler: roothandler) }
      
        var interceptedMessages: [Message] = []                                                                             ; afterEach { interceptedMessages = [] }
        var roothandler        : ((Message) -> ())!; beforeEach { roothandler = { msg in interceptedMessages.append(msg) } }; afterEach { roothandler = nil        }
        var appCore            : Input!;             beforeEach { appCore = appcore(with: roothandler) }                    ; afterEach { appCore     = nil        }

        describe("AppCore") {
            context("turn light") {
                context("on") { beforeEach { appCore(.lighting(.turn(thelights.first!, .on))) }
                    it("succeeds") { expect(interceptedMessages).toEventually(equal([Message.lighting(.turning(thelights.first!, .on, .succeeded))])) } }
                context("off") { beforeEach { appCore(.lighting(.turn(thelights.first!, .off))) }
                    it("succeeds") {
                        let expectedLight = thelights.first!.alter(by: .turning(.it(.off)))
                        expect(interceptedMessages).toEventually(equal([Message.lighting(.turning(expectedLight, .off, .succeeded))])) } }
            }
        }
    }
}
