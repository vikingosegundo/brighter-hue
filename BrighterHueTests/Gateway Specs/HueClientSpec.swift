//
//  HueClientSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 20/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
@testable import BrighterHue
import BrighterModel

final
class HueClientSpec: QuickSpec {
    override func spec() {
        var client:HueClient!; beforeEach { client = HueClient(ip:"127.0.0.1:8090", user: "uuu") }; afterEach { client = nil }
        var server:MockServer!
        describe("HueClient") {
            server = MockServer()
            server.start()
            var fetchedLights: [Light] = []; afterEach { fetchedLights = [] }
            var fetchedRooms : [Room ] = []; afterEach { fetchedRooms = []  }
            context("fetching") {
                beforeEach {
                    client.rooms  { switch $0 { case .success(let rooms ): fetchedRooms  = rooms;  case .failure(_): break } }
                    client.lights { switch $0 { case .success(let lights): fetchedLights = lights; case .failure(_): break } } }
                afterEach { fetchedLights = []; fetchedRooms = [] }
                context("lights") { it("succeeds") { expect(fetchedLights).toEventually(haveCount(10))} }
                context("rooms" ) { it("succeeds") { expect(fetchedRooms ).toEventually(haveCount(11))} }
            }
            context("fetching") {
                context("lights") {
                    beforeEach { server.shouldFail = true ; fetchedLights = [Light(id: "01", name: "nope")] };
                    afterEach  { server.shouldFail = false; fetchedLights = []                              }
                    beforeEach {
                        client.rooms  {           switch $0 { case .success(let rooms ): fetchedRooms  = rooms;  case .failure(_): break} }
                        client.lights { print($0);switch $0 { case .success(let lights): fetchedLights = lights; case .failure(_): break} }
                    }
                    it("fails") { expect(fetchedLights).toEventually(haveCount(0)) } }
            }
            context("toggling") {
                context("light") {
                    var toggeledLight: Light! = Light(id: "00", name: "00")
                    context("on") {
                        beforeEach {
                            client.toggle(light: Light(id: "36", name: "thirty6"), .on) {
                                switch $0 {
                                case .success(let light): toggeledLight = light
                                case .failure(_): ()
                                }
                            }
                        }
                        it("fails") { expect(toggeledLight.isOn).toEventually(equal(.on)) }
                    }
                    context("off") {
                        beforeEach {
                            client.toggle(light: Light(id: "36", name: "thirty6"), .off) {
                                switch $0 {
                                case .success(let light): toggeledLight = light
                                case .failure(_): ()
                                }
                            }
                        }
                        it("succeed") { expect(toggeledLight).toNotEventually(beNil()) }
                    }
                }
            }
            context("renaming light") {
                var renamedLight: Light!
                var error: LightError!
                beforeEach {
                    client.rename(light: Light(id: "36", name: "thirty6"), to: "30six") {
                        switch $0 {
                        case .success(let light): renamedLight = light
                        case .failure(let e): error = e
                        }
                    }
                }
                it("succeed") { expect(renamedLight.name).toEventually(equal("30six")) }
                it("no error expected") { expect(error).toEventually(beNil()) }
            }
            context("setting values") {
                context("hsb") {
                    var light: Light!
                    var error: LightError!
                    beforeEach {
                        client.set(values: .hsb(0.5, 0.5, 0.5), on: Light(id: "36", name: "thirty6")) {
                            switch $0 {
                            case .success(let l): light = l
                            case .failure(let e): error = e
                            }
                        }
                    }
                    it("succeed for hue"       ) { expect(light?.hue       ).toEventually(equal(0.5)) }
                    it("succeed for saturation") { expect(light?.saturation).toEventually(equal(0.5)) }
                    it("succeed for brightness") { expect(light?.brightness).toEventually(equal(0.5)) }
                    it("no error expected"     ) { expect(error).toEventually(beNil()) }
                }
                context("ct") {
                    var light: Light!
                    var error: LightError!
                    beforeEach {
                        client.set(values: .ct(180, 0.75), on: Light(id: "36", name: "thirty6")) {
                            switch $0 {
                            case .success(let l): light = l
                            case .failure(let e): error = e
                            }
                        }
                    }
                    it("succeed for hue"       ) { expect(light?.ct        ).toEventually(equal(180)) }
                    it("succeed for brightness") { expect(light?.brightness).toEventually(equal(0.75)) }
                    it("no error expected"     ) { expect(error).toEventually(beNil()) }
                }
                context("brightness") {
                    var light: Light!
                    var error: LightError!
                    beforeEach {
                        client.set(values: .bri(0.7), on: Light(id: "36", name: "thirty6")) {
                            switch $0 {
                            case .success(let l): light = l
                            case .failure(let e): error = e
                            }
                        }
                    }
                    it("succeed for brightness") { expect(light?.brightness).toEventually(equal(0.7)) }
                    it("no error expected"     ) { expect(error).toEventually(beNil()) }
                }
            }
        }
    }
}
