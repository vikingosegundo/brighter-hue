//
//  RoomSpec.swift
//  BrighterHueTests
//
//  Created by Manuel Meyer on 25.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
import BrighterModel

@testable import BrighterHue

final
class RoomSpec: QuickSpec {
    override func spec() {
        describe("creating a room"       ) { let room = Room(id:"01",title:"aquarium")
            it("it will be given an id"  ) { expect(room.id   ).to(equal("01"))        }
            it("it will be given a title") { expect(room.title).to(equal("aquarium"))  } }
        describe("altering a room"       ) { let room = Room(id:"01",title:"aquarium")
            context("by renaming it"     ) { let room = room.alter(.title("fish tank"))
                it("it will be renamed"  ) { expect(room.title).to(equal("fish tank")) } } }
    }
}
