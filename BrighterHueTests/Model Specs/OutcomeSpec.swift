//
//  OutcomeSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 24/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
import BrighterModel

@testable import BrighterHue

final
class OutcomeSpec: QuickSpec {
    override func spec() {
        describe("Outcome") {
            context("comparision") {
                    
                    it("") {
                        expect(Outcome.succeeded == .succeeded) == true
                        expect(Outcome.succeeded == .failed(nil)) == false
                        expect(Outcome.failed(nil) == .succeeded) == false
                        expect(Outcome.failed(nil) == .failed(nil)) == true
                        expect(Outcome.failed(LightError.unknown) == .failed(LightError.lightsEndpoint(nil))) == false

                }
            }
        }
    }
}
