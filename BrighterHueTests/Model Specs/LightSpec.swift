//
//  LightSpec.swift
//  BrighterHueTests
//
//  Created by Manuel Meyer on 25.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
@testable import BrighterHue
import BrighterModel

final
class LightSpec: QuickSpec {
    override func spec() {
        let light = Light(id:"01", name:"desk")
        describe("creating a light") {
            it("will be given an id"                 ) { expect(light.id                  ).to(equal("01"  ))  }
            it("will be given a name"                ) { expect(nameValue       (of:light)).to(equal("desk"))  }
            it("will be given a ct value"            ) { expect(temperatureValue(of:light)).to(equal(0))       }
            it("will be given a hue value"           ) { expect(hueValue        (of:light)).to(equal(0))       }
            it("will be given a saturation value"    ) { expect(saturationValue (of:light)).to(equal(0))       }
            it("will be given a brightness value"    ) { expect(brightnessValue (of:light)).to(equal(0))       }
            it("will be turend off"                  ) { expect(light.isOn                ).to(equal(.off))    }
            it("will be set to slider display"       ) { expect(light.display             ).to(equal(.slider)) }
            it("will be set up without selected mode") { expect(modeValue       (of:light)).to(equal(.unset))  } }
        describe("altering a light") {
            context("by renaming"                                      ) { let light = alter(light, by:.renaming(.it(to:"kitchen")))
                it("it will be given a new name"                       ) {   expect(light.name).to(equal("kitchen"))                        } }
            context("by changing") {
                context("color temperature"                            ) { let light = alter(light, by:.setting(.temperature(to:200.mk)))
                    it("it will be given a new ct value"               ) {   expect(temperatureValue(of: light)).to(equal(200))             }
                    it("it will have selected mode ct"                 ) {   expect(modeValue(of:light)).to(equal(.ct))                     }
                    context("beyond lower boundary"                    ) { let light = alter(light, by:.setting(.temperature(to:0.mirek)))
                        it("it will be corrected to lower bound"       ) {   expect(temperatureValue(of: light)).to(equal(153))             }
                        it("it will be set to mode ct"                 ) {   expect(modeValue(of:light)).to(equal(.ct))                     } }
                    context("beyond upper boundary"                    ) { let light = alter(light, by:.setting(.temperature(to:1000.mk)))
                        it("it will be corrected to upper bound"       ) {   expect(temperatureValue(of: light)).to(equal( 500))            } } }
                context("hue"                                          ) { let light = alter(light, by:.setting(.hue(to:0.5)))
                    it("it will be given a hue value"                  ) {   expect( hueValue(of:light)).to(equal(0.5))                     }
                    it("it will have selected mode hsb"                ) {   expect(modeValue(of:light)).to(equal(.hsb))                    } }
                context("saturation"                                   ) { let light = alter(light, by:.setting(.saturation(to:0.5)))
                    it("it will be given a new hue value"              ) {   expect(saturationValue(of:light)).to(equal(0.5))               }
                    it("it will have hsb selected"                     ) {   expect(modeValue(of:light)).to(equal(.hsb))                    } }
                context("brightness"                                   ) { let light = alter(light, by:.setting(.brightness(to:0.5)))
                    it("it will be given a new brightness value"       ) {   expect(brightnessValue(of:light)).to(equal(0.5))               }
                    it("it will no selected mode"                      ) {   expect(modeValue(of:light)).to(equal(.unset))                  } }
                context("display") {
                    it("defaults to slider"                            ) {   expect(light.display).to(equal(.slider))                       }
                    context("changing to scrubbing"                    ) { let light = alter(light, by:(.toggling(.display(to:.scrubbing))))
                        it("will change light's interface to scrubbing") {   expect(light.display).to(equal(.scrubbing))                    }
                        it("it will no selected mode"                  ) {   expect(modeValue(of:light)).to(equal(.unset))                  } } } }
            context("by turning it") {
                context("on"                                           ) { let light = alter(light, by:.turning(.it(.on)))
                    it("it will be turned on"                          ) {     expect(light.isOn).to(equal(.on))            }
                    context("and off again"                            ) { let light = alter(light, by:.turning(.it(.off)))
                        it("it will be turned off"                     ) {     expect(light.isOn).to(equal(.off))           } } } } }
        describe("Light.Mode"                              ) { let i0 = systemImage(for: .hsb); let i1 = systemImage(for: .ct); let i2 = systemImage(for: .unset)
            it("loads image for hsb"                       ) { expect(i0).toNot(beNil())                                                            }
            it("loads image for ct"                        ) { expect(i1).toNot(beNil())                                                            }
            it("loads image for unser"                     ) { expect(i2).toNot(beNil())                                                            }
            it("loads different images for different cases") { expect(i0).toNot(equal(i1));expect(i1).toNot(equal(i2)); expect(i0).toNot(equal(i2)) } }
    }
}
