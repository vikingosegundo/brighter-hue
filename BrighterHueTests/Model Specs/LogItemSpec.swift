//
//  LogItemSpec.swift
//  BrighterHueTests
//
//  Created by vikingosegundo on 24/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
import BrighterModel

@testable import BrighterHue

final
class LogItemSpec: QuickSpec {
    override func spec() {
        describe("LogItem") {
            var logItem: LogItem!
            
            context("create item") {
                beforeEach { logItem = LogItem(text: "Hello World") }
                it("successfully with text") { expect(logItem.text) == "Hello World"        }
                it("successfully with date") { expect(logItem.date).to(beCloseTo(Date(), within: 0.01))   }
            }
        }
    }
}
