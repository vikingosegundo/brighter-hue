//
//  BrighterHueApp.swift
//  BrighterHueWatch Extension
//
//  Created by Manuel Meyer on 05.07.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI

@main
struct BrighterHueApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
