//
//  ContentView.swift
//  BrighterHueWatch Extension
//
//  Created by Manuel Meyer on 05.07.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
