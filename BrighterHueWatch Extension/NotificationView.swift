//
//  NotificationView.swift
//  BrighterHueWatch Extension
//
//  Created by Manuel Meyer on 05.07.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
