//
//  BrighterHueApp.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import BrighterUI
import BrighterModel

fileprivate let store      : Store           = createDiskStore()
fileprivate let viewState  : ViewState       = ViewState(store:store)
fileprivate let rootHandler: (Message) -> () = createAppDomain(
    store:store,
    receivers:[
        viewState.handle(msg:),
        expandLoggingMessages(rootHandler:{ rootHandler($0) }) ],
    rootHandler:{ rootHandler($0) }
)
@main
struct BrighterHueApp: App {
    init() {
        rootHandler(.lighting(.load(.lights)))
        rootHandler(.lighting(.load(.rooms)))
    }
    var body: some Scene { WindowGroup { ContentView( viewState:viewState, rootHandler:rootHandler ) } }
}

fileprivate
func expandLoggingMessages(rootHandler: @escaping (Message) -> ()) -> (Message) -> () {
    {
        if case let .lighting(.turning(l, onOrOff, .succeeded)) = $0 { rootHandler(.logging(.write(LogItem(text: "light \(l.name) was turned \(onOrOff == .on ? "on" : "off") successfully"  )))) }
        if case let .lighting(.turning(l, onOrOff, .failed(_))) = $0 { rootHandler(.logging(.write(LogItem(text: "light \(l.name) was turned \(onOrOff == .on ? "on" : "off") unsuccessfully")))) }
    }
}
