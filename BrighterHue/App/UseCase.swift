//
//  UseCase.swift
//  SpokeChat
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

protocol UseCase {
    associatedtype RequestType
    associatedtype ResponseType
    
    func request(_ request:RequestType)
//  init(..., responder:@escaping (ResponseType) -> ())
}
