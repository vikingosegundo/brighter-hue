//
//  createAppDomain.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Foundation
import BrighterModel

typealias  Input = (Message) -> ()
typealias Output = (Message) -> ()
typealias AppStore = Store<AppState,AppState.Change>

enum StackKind { case hue, mock }

func createAppDomain(
    store      : AppStore,
    receivers  : [Input],
    stackKind  : StackKind = .mock,
    rootHandler: @escaping Output) -> Input
{
    store.updated {
#if targetEnvironment(simulator)
        print("\(state(in: store))")
#endif
    }
    let stack = stack(of: stackKind)
    let features: [Input] = [
        createLightingFeature (store:store, lightsStack:stack, output:rootHandler),
        createDashboardFeature(store:store,                    output:rootHandler),
        createLoggingFeature()
    ]
    return { msg in
        (receivers + features).forEach { $0(msg) }
    }
}

fileprivate func stack(of kind:StackKind) -> LightsStack {
    switch kind {
    case .hue : return LightsStack(client: HueClient(ip:"192.168.178.37", user:"0cpSVXBH47AspTz8yz6xzPRH4e4DHIktuE18IUhd"))
    case .mock: return LightsStack(client: MockClient(with: thelights, rooms: []                                         ))
    }
}
