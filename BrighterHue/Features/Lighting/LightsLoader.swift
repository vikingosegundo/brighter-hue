//
//  LightsLoader.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 12.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel

func request(_ loader: LightsLoader, to r: LightsLoader.Request) { loader.request(r) }

struct LightsLoader:UseCase {
    enum Request  { case load   (_Load); enum _Load { case all(Items) } }
    enum Response { case loading(Loaded, Outcome)                       }
    
    init(lightsStack:LightsStack, store:AppStore, responder:@escaping (Response) -> ()) {
        self.lightStack = lightsStack
        self.store      = store
        self.respond    = responder
    }
    
    func request(_ request: Request) {
        switch request {
        case .load(.all(.lights)):
            lightStack.lights {
            switch $0 {
            case .success(let lights): change(store, .by(.replacing(.lights(with:lights)))); respond(.loading(.lights(lights), .succeeded))
            case .failure(let e)     :                                                       respond(.loading(.lights( [  ] ), .failed(e)))
            }
        }
        case .load(.all(.rooms)):
            lightStack.rooms {
                switch $0 {
                case .success(let rooms): change(store, .by(.replacing(.rooms(with:rooms)))); respond(.loading(.rooms(rooms), .succeeded))
                case .failure(let e)    :                                                     respond(.loading(.rooms( [ ] ), .failed(e)))
                }
            }
        }
    }
    
    private let lightStack: LightsStack
    private let store     : AppStore
    private let respond   : (Response) -> ()
    
    typealias RequestType  = Request
    typealias ResponseType = Response
}


// MARK: - Executable Documentation
#if targetEnvironment(simulator)
extension LightsLoader {
    static func Documentation() -> [(LightsLoader.Request, LightsLoader.Response, LightsLoader.Response)] {
        let l = Light(id:"02", name:"oo")
        let r = Room(id: "r1", title: "r1")
        return [
          // |--- LL.Request ---|   |<- LightsLoader.Response Scc ->|   |< LightsLoader.Response Failure >|
            (.load(.all(.lights)),  .loading(.lights([l]),.succeeded),  .loading(.lights([]), .failed(nil))),
            (.load(.all(.rooms )),  .loading(.rooms ([r]),.succeeded),  .loading(.rooms ([]), .failed(nil)))
        ]
    }
}
#endif
