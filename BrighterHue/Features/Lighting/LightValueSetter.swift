//
//  LightValueSetter.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 12.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//
import BrighterModel

func request(_ setter:LightValueSetter, to r: LightValueSetter.Request) { setter.request(r) }

struct LightValueSetter: UseCase {
    typealias RequestType  = Request
    typealias ResponseType = Response
    enum Request  { case apply   (Apply, on: Light)          }
    enum Response { case applying(Apply, on: Light, Outcome) }
    
    private let lightsStack : LightsStack
    private let store       : AppStore
    private let respond     : (Response) -> ()
    
    init(lightsStack:LightsStack, store:AppStore, responder:@escaping (Response) -> ()) {
        self.lightsStack = lightsStack
        self.store       = store
        self.respond     = responder
    }
    
    func request(_ request: Request) {
        switch request {
        case let .apply(.values(v),on:l): lightsStack.set(values:v,on:l) {
            switch $0 {
            case .success(let l): store.change(.by(.replacing(.light(with:l)))); respond(.applying(.values(v),on:l,.succeeded))
            case .failure(let e):                                                respond(.applying(.values(v),on:l,.failed(e)))
            }
        }
        }
    }
}

// MARK: - Executable Documentation
#if targetEnvironment(simulator)
extension LightValueSetter {
    static func Documentation() -> [(LightValueSetter.Request, LightValueSetter.Response, LightValueSetter.Response)] {
        let l = Light(id:"02", name:"oo")
        return [
         //  |------ LightValueSetter.Request -----|    |<------ LightValueSetter.Response Success -------->|   |<-------- LightValueSetter.Response Failure -------->|
            (.apply(.values(.bri(0.75)),       on:l),   .applying(.values(.bri(0.75)),       on:l,.succeeded),  .applying(.values(.bri(0.75)),       on:l,.failed(nil))),
            (.apply(.values(.hsb(0.7,0.8,0.9)),on:l),   .applying(.values(.hsb(0.7,0.8,0.9)),on:l,.succeeded),  .applying(.values(.hsb(0.7,0.8,0.9)),on:l,.failed(nil))),
            (.apply(.values(.ct(200,0.75)),    on:l),   .applying(.values(.ct(200,0.75)),    on:l,.succeeded),  .applying(.values(.ct(200,0.75)),    on:l,.failed(nil))),
        ]
    }
}
#endif
