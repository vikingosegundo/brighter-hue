//
//  LightChanger.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 23.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel

func request(_ changer:LightChanger, to r:LightChanger.Request) { changer.request(r) }

struct LightChanger: UseCase {
    enum Request  { case change  (Alter,     on:Light)                                                                    }
    enum Response { case changing(_Changing, on:Light, Outcome); enum _Changing { case title(String), display(Interface) } }
    
    init(lightsStack:LightsStack, store:AppStore, responder:@escaping (Response) -> ()) {
        self.lightStack = lightsStack
        self.store      = store
        self.respond    = responder
    }
    
    func request(_ request:Request) {
        switch request {
        case let .change(.name(name), on:light):
            lightStack.rename(light: light, to:name) {
                switch $0 {
                case .success(let light): change(store, .by(.replacing(.light(with:light))) ); respond( .changing(.title(name),on:light,.succeeded) )
                case .failure(let e    ):                                                      respond( .changing(.title(name),on:light,.failed(e)) )
                }
            }
        case let .change(.display(interface),on:light):
            change (store, .by(.replacing(.light(with:alter(light, by:.toggling(.display(to:interface)))))) )
            respond( .changing(.display(interface), on: light, .succeeded) )
        }
    }
    
    private let lightStack: LightsStack
    private let store     : AppStore
    private let respond   : (Response) -> ()
    
    typealias  RequestType = Request
    typealias ResponseType = Response
}

// MARK: - Executable Documentation
#if targetEnvironment(simulator)
extension LightChanger {
    static func Documentation() -> [(LightChanger.Request, LightChanger.Response, LightChanger.Response)] {
        let l = Light(id:"02", name:"oo")
        return [
          // |----- LightChanger.Request -----|   |<----- LightChanger.Response Succeess ------>|   |<------- LightChanger.Response Failure ------->|
            (.change(.name("02+"),        on:l),  .changing(.title("02+"),       on:l,.succeeded),  .changing(.title("02+"),       on:l,.failed(nil))),
            (.change(.display(.scrubbing),on:l),  .changing(.display(.scrubbing),on:l,.succeeded),  .changing(.display(.scrubbing),on:l,.failed(nil))),
            (.change(.display(.slider),   on:l),  .changing(.display(.slider),   on:l,.succeeded),  .changing(.display(.slider),   on:l,.failed(nil)))
        ]
    }
}
#endif
