//
//  LightSwitcher.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 12.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel

func request(_ switcher: LightSwitcher, to r: LightSwitcher.Request)  { switcher.request(r) }

struct LightSwitcher: UseCase {
    typealias RequestType = Request
    typealias ResponseType = Response
    
    enum Request  {
        case turn (Light, Turn)
    }
    enum Response {
        case light(Light, Turned); 
        enum Turned {
            case was(turned:Turn)
        }
    }
    
    private let lightsStack: LightsStack
    private let store      : AppStore
    private let respond    : (Response) -> ()
    
    init(lightsStack:LightsStack, store:AppStore, responder:@escaping (Response) -> ()) {
        self.lightsStack = lightsStack
        self.store       = store
        self.respond     = responder
    }
    
    func request(_ request: Request) {
        switch request {
        case let .turn( light, onOrOff):
            lightsStack.toggle(light:light,onOrOff) {
                switch $0 {
                case .success(let light):
                    store.change(.by(.replacing(.light(with:light.alter(by:.turning(.it(onOrOff)))))))
                    respond( .light(light,.was(turned:onOrOff)) )
                case .failure(_): break
                }
            }
        }
    }
}

// MARK: - Executable Documentation
#if targetEnvironment(simulator)
extension LightSwitcher {

    static func Documentation() -> [(LightSwitcher.Request, LightSwitcher.Response)] {

        let l = Light(id:"02", name:"oo")
        return [
         // |-LS.Request-|  |<LightSwitcher.Response >|
            (.turn(l,.on ), .light(l,.was(turned:.on ))),
            (.turn(l,.off), .light(l,.was(turned:.off))),
        ]
    }
}
#endif
