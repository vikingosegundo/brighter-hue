//
//  Increaser.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 18.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel

func request(_ dimmer:Dimmer, to r:Dimmer.Request) { dimmer.request(r) }

struct Dimmer: UseCase {
    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let lightsStack: LightsStack
    private let store      : AppStore
    private let respond    : (Response) -> ()
    
    enum Request  { case increase(Light.Value, by:Light.Value.Increment, on:Light)
                    case decrease(Light.Value, by:Light.Value.Increment, on:Light) }
    enum Response { case increase(Light.Value, by:Light.Value.Increment, on:Light, Outcome)
                    case decrease(Light.Value, by:Light.Value.Increment, on:Light, Outcome) }
    init(lightsStack:LightsStack, store:AppStore, responder:@escaping (Response) -> ()) {
        self.lightsStack = lightsStack
        self.store       = store
        self.respond     = responder
    }

    func request(_ request: Request) {
        switch request{
        case let .increase(.brightness, by:.pt(increase), on:light):
            lightsStack.set(values:.bri(min(1.00, brightnessValue(of:light) + (increase / 100.0))), on:light) { result in
                switch result {
                case .success(let light): store.change(.by(.replacing(.light(with:light)))); respond( .increase(.brightness,by:.pt(increase),on:light,.succeeded)     )
                case .failure(let error):                                                    respond( .increase(.brightness,by:.pt(increase),on:light,.failed(error)) )
                }
            }
        case let .decrease(.brightness, by:.pt(decrease), on:light):
            lightsStack.set(values:.bri(max(0.01, brightnessValue(of:light) - (decrease / 100.0))), on:light) { result in
                switch result {
                case .success(let light): store.change(.by(.replacing(.light(with:light)))); respond( .decrease(.brightness,by:.pt(decrease),on:light,.succeeded)     )
                case .failure(let error):                                                    respond( .decrease(.brightness,by:.pt(decrease),on:light,.failed(error)) )
                }
            }
        case let .increase(.saturation, by:.pt(increase), on:light):
            lightsStack.set(values:.hsb(hueValue(of:light), min(1.00, saturationValue(of:light) + (increase / 100.0)), brightnessValue(of:light)), on:light) { result in
                switch result {
                case .success(let light): store.change(.by(.replacing(.light(with:light)))); respond( .increase(.saturation,by:.pt(increase),on:light,.succeeded)     )
                case .failure(let error):                                                    respond( .increase(.saturation,by:.pt(increase),on:light,.failed(error)) )
                }
            }
        case let .decrease(.saturation, by:.pt(decrease), on:light):
            lightsStack.set(values:.hsb(hueValue(of:light), max(0.01, saturationValue(of:light) - (decrease / 100.0)), brightnessValue(of:light)), on:light) { result in
                switch result {
                case .success(let light): store.change(.by(.replacing(.light(with:light)))); respond( .decrease(.saturation,by:.pt(decrease),on:light,.succeeded)     )
                case .failure(let error):                                                    respond( .decrease(.saturation,by:.pt(decrease),on:light,.failed(error)) )
                }
            }
        case let .increase(.hue, by:.pt(increase), on:light):
            lightsStack.set(values:.hsb(min(1.00, hueValue(of:light) + (increase / 100.0)), saturationValue(of:light), brightnessValue(of:light)), on:light) { result in
                switch result {
                case .success(let light): store.change(.by(.replacing(.light(with:light)))); respond( .increase(.hue,by:.pt(increase),on:light,.succeeded)     )
                case .failure(let error):                                                    respond( .increase(.hue,by:.pt(increase),on:light,.failed(error)) )
                }
            }
        case let .decrease(.hue, by:.pt(decrease), on:light):
            lightsStack.set(values:.hsb(max(0.01, hueValue(of:light) - (decrease / 100.0)), saturationValue(of:light), brightnessValue(of:light)), on:light) { result in
                switch result {
                case .success(let light): change(store, .by(.replacing(.light(with:light)))); respond( .decrease(.hue,by:.pt(decrease),on:light,.succeeded)     )
                case .failure(let error):                                                     respond( .decrease(.hue,by:.pt(decrease),on:light,.failed(error)) )
                }
            }
        case let .increase(.colortemp, by:.pt(increase), on:light):
            lightsStack.set(values:.ct( min(500, temperatureValue(of:light) + Int(increase * ((500-153) / 100))), brightnessValue(of:light)), on:light) { result in
                switch result {
                case .success(let light): store.change(.by(.replacing(.light(with:light)))); respond( .increase(.colortemp,by:.pt(increase),on:light,.succeeded)     )
                case .failure(let error):                                                    respond( .increase(.colortemp,by:.pt(increase),on:light,.failed(error)) )
                }
            }
        case let .decrease(.colortemp, by:.pt(decrease), on:light):
            lightsStack.set(values:.ct( max(153, temperatureValue(of:light) - Int(decrease * ((500-153) / 100))), brightnessValue(of:light)), on:light) { result in
                switch result {
                case .success(let light): store.change(.by(.replacing(.light(with:light)))); respond( .decrease(.colortemp,by:.pt(decrease),on:light,.succeeded)     )
                case .failure(let error):                                                    respond( .decrease(.colortemp,by:.pt(decrease),on:light,.failed(error)) )
                }
            }
        }
    }
}
// MARK: - Executable Documentation
#if targetEnvironment(simulator)
extension Dimmer {
    static func Documentation() -> [(Dimmer.Request, Dimmer.Response, Dimmer.Response)] {
        let l = Light(id:"02", name:"oo")
        return [
          // |-------- Dimmer.Request ----------|   |<-------- Dimmer.Response Succeess --------->|     |<---------- Dimmer.Response Failure ---------->|
            (.increase(.hue,       by:10.pt,on:l),  .increase(.hue,       by:10.pt,on:l,.succeeded),    .increase(.hue,       by:10.pt,on:l,.failed(nil))),
            (.increase(.saturation,by:10.pt,on:l),  .increase(.saturation,by:10.pt,on:l,.succeeded),    .increase(.saturation,by:10.pt,on:l,.failed(nil))),
            (.increase(.brightness,by:10.pt,on:l),  .increase(.brightness,by:10.pt,on:l,.succeeded),    .increase(.brightness,by:10.pt,on:l,.failed(nil))),
            (.increase(.colortemp, by:10.pt,on:l),  .increase(.colortemp, by:10.pt,on:l,.succeeded),    .increase(.colortemp, by:10.pt,on:l,.failed(nil))),
            (.decrease(.hue,       by:10.pt,on:l),  .decrease(.hue,       by:10.pt,on:l,.succeeded),    .decrease(.hue,       by:10.pt,on:l,.failed(nil))),
            (.decrease(.saturation,by:10.pt,on:l),  .decrease(.saturation,by:10.pt,on:l,.succeeded),    .decrease(.saturation,by:10.pt,on:l,.failed(nil))),
            (.decrease(.brightness,by:10.pt,on:l),  .decrease(.brightness,by:10.pt,on:l,.succeeded),    .decrease(.brightness,by:10.pt,on:l,.failed(nil))),
            (.decrease(.colortemp, by:10.pt,on:l),  .decrease(.colortemp, by:10.pt,on:l,.succeeded),    .decrease(.colortemp, by:10.pt,on:l,.failed(nil)))
        ]
    }
}
#endif
