//  Copyright © 2021 Manuel Meyer. All rights reserved.

import BrighterModel

func createLightingFeature(store      : AppStore,
                           lightsStack: LightsStack,
                           output     : @escaping Output) -> Input
{
    let loader      = LightsLoader    (lightsStack:lightsStack,store:store,responder:handle(output:output))
    let switcher    = LightSwitcher   (lightsStack:lightsStack,store:store,responder:handle(output:output))
    let valueSetter = LightValueSetter(lightsStack:lightsStack,store:store,responder:handle(output:output))
    let dimmer      = Dimmer          (lightsStack:lightsStack,store:store,responder:handle(output:output))
    let changer     = LightChanger    (lightsStack:lightsStack,store:store,responder:handle(output:output))
    
    func execute(command cmd: Message._Lighting) {
        //          |---------- Global Command ---------|         request |- UseCase -| to |<-------- UseCase Request -------->|
        if case let .load(lightsOrRooms)                  = cmd { request( loader,      to:.load(.all(lightsOrRooms))            ) }
        if case let .turn(light, onOrOff)                 = cmd { request( switcher,    to:.turn(light, onOrOff )                ) }
        if case let .apply(config,              on:light) = cmd { request( valueSetter, to:.apply(config,              on:light) ) }
        if case let .increase(value,by:increase,on:light) = cmd { request( dimmer,      to:.increase(value,by:increase,on:light) ) }
        if case let .decrease(value,by:decrease,on:light) = cmd { request( dimmer,      to:.decrease(value,by:decrease,on:light) ) }
        if case let .change(.name(name),        on:light) = cmd { request( changer,     to:.change(.name(name),        on:light) ) }
        if case let .change(.display(interface),on:light) = cmd { request( changer,     to:.change(.display(interface),on:light) ) }
    }
    return { // entry point
        if case .lighting(let c) = $0 { execute(command:c) } // execute if lighting is command's recipient
    }
}
// MARK: - UseCase response handler
private func handle(output: @escaping Output) -> (   LightSwitcher.Response) -> () { { process(response:$0,output:output) } }
private func handle(output: @escaping Output) -> (LightValueSetter.Response) -> () { { process(response:$0,output:output) } }
private func handle(output: @escaping Output) -> (          Dimmer.Response) -> () { { process(response:$0,output:output) } }
private func handle(output: @escaping Output) -> (    LightChanger.Response) -> () { { process(response:$0,output:output) } }
private func handle(output: @escaping Output) -> (    LightsLoader.Response) -> () { { process(response:$0,output:output) } }

private func process(response:   LightSwitcher.Response, output: @escaping Output) { switch response { case let .light   (light,.was(turned:onOrOff)      ): output( .lighting(.turning (light, onOrOff,.succeeded       )) ) }}
private func process(response:LightValueSetter.Response, output: @escaping Output) { switch response { case let .applying(values,on:light,succeededOrFaild): output( .lighting(.applying(values,on:light,succeededOrFaild)) ) }}
private func process(response:    LightsLoader.Response, output: @escaping Output) { switch response { case let .loading (lightsOrRooms,  succeededOrFaild): output( .lighting(.loading (lightsOrRooms,  succeededOrFaild)) ) }}
private func process(response:          Dimmer.Response, output: @escaping Output) {
    switch response {
    case let .increase(value,by:increment,on:light,succeededOrFaild): output( .lighting(.increasing(value,by:increment,on:light,succeededOrFaild)) )
    case let .decrease(value,by:increment,on:light,succeededOrFaild): output( .lighting(.decreasing(value,by:increment,on:light,succeededOrFaild)) )
    }
}
private func process(response:    LightChanger.Response, output: @escaping Output) {
    switch response {
    case let .changing(.title(name),       on:light,succeededOrFaild): output( .lighting(.changing(.name(name),        on:light,succeededOrFaild)) )
    case let .changing(.display(interface),on:light, _              ): output( .lighting(.changing(.display(interface),on:light,.succeeded      )) )
    }
}
// MARK: - Executable Documentation
#if targetEnvironment(simulator)
enum LightingFeature {
    static func Documentation ()  -> [(Message._Lighting,Message._Lighting,Message._Lighting)]{
        let l = Light(id: "1", name: "1")
        let r = Room(id: "a", title: "a")
        let e = LightError.unknown
        let docs: [(Message._Lighting,Message._Lighting,Message._Lighting)] =
        // These are examples for every lighting command and their possible responses. These columns do show all commands the Lighting Module
        // will understand and that are constructable. So to speak: it is the complete vocabulary.
        // The first column contains commands that the user triggers directly or indirectly.
        // The second column shows responses for the success case and
        // the third column shows reponses for the failure case.
        [ //|----------------- Command ----------------|    |------------------- Response Success ----------------|     |----------------- Response Failure -------------------|
            (.load    (.lights),                            .loading   (.lights([l]),                   .succeeded),    .loading   (.lights([]),                    .failed(e))),
            (.load    (.rooms ),                            .loading   (.rooms ([r]),                   .succeeded),    .loading   (.rooms ([]),                    .failed(e))),
            (.turn    (l,.on  ),                            .turning   (l,.on,                          .succeeded),    .turning   (l,.on,                          .failed(e))),
            (.turn    (l,.off ),                            .turning   (l,.off,                         .succeeded),    .turning   (l,.off,                         .failed(e))),
            (.apply   (.values(.ct (  200,  0.5)),on:l),    .applying  (.values(.ct (  200,  0.5)),on:l,.succeeded),    .applying  (.values(.ct (  200,  0.5)),on:l,.failed(e))),
            (.apply   (.values(.hsb(0.5,0.5,0.5)),on:l),    .applying  (.values(.hsb(0.5,0.5,0.5)),on:l,.succeeded),    .applying  (.values(.hsb(0.5,0.5,0.5)),on:l,.failed(e))),
            (.apply   (.values(.bri(        0.5)),on:l),    .applying  (.values(.bri(        0.5)),on:l,.succeeded),    .applying  (.values(.bri(        0.5)),on:l,.failed(e))),
            (.increase(.colortemp, by:10.pt,      on:l),    .increasing(.colortemp, by:10.pt,      on:l,.succeeded),    .increasing(.colortemp, by:10.pt,      on:l,.failed(e))),
            (.increase(.hue,       by:10.pt,      on:l),    .increasing(.hue,       by:10.pt,      on:l,.succeeded),    .increasing(.hue,       by:10.pt,      on:l,.failed(e))),
            (.increase(.saturation,by:10.pt,      on:l),    .increasing(.saturation,by:10.pt,      on:l,.succeeded),    .increasing(.saturation,by:10.pt,      on:l,.failed(e))),
            (.increase(.brightness,by:10.pt,      on:l),    .increasing(.brightness,by:10.pt,      on:l,.succeeded),    .increasing(.brightness,by:10.pt,      on:l,.failed(e))),
            (.decrease(.colortemp, by:10.pt,      on:l),    .decreasing(.colortemp, by:10.pt,      on:l,.succeeded),    .decreasing(.colortemp, by:10.pt,      on:l,.failed(e))),
            (.decrease(.hue,       by:10.pt,      on:l),    .decreasing(.hue,       by:10.pt,      on:l,.succeeded),    .decreasing(.hue,       by:10.pt,      on:l,.failed(e))),
            (.decrease(.saturation,by:10.pt,      on:l),    .decreasing(.saturation,by:10.pt,      on:l,.succeeded),    .decreasing(.saturation,by:10.pt,      on:l,.failed(e))),
            (.decrease(.brightness,by:10.pt,      on:l),    .decreasing(.brightness,by:10.pt,      on:l,.succeeded),    .decreasing(.brightness,by:10.pt,      on:l,.failed(e))),
            (.change  (.name("1a"),               on:l),    .changing  (.name("1a"),               on:l,.succeeded),    .changing  (.name("1a"),               on:l,.failed(e))),
            (.change  (.display(.slider),         on:l),    .changing  (.display(.slider),         on:l,.succeeded),    .changing  (.display(.slider),         on:l,.failed(e))),
            (.change  (.display(.scrubbing),      on:l),    .changing  (.display(.scrubbing),      on:l,.succeeded),    .changing  (.display(.scrubbing),      on:l,.failed(e))),
        ]
        
//      SwiftUI Example:
//      Button { roothandler( .lighting(.decrease(.hue,by:10.pt,on:l)) ) } label: { Image(systemName:"paintpalette") }
        return docs
    }
}
#endif
