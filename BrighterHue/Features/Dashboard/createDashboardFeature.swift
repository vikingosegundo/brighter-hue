//
//  createDashboardFeature.swift
//  BrighterHue
//
//  Created by vikingosegundo on 02/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel

func createDashboardFeature(  store: AppStore,
                             output: @escaping Output) -> Input
{
    let adder = FavoriteAdder(store: store) { response in
        switch response {
        case let .favorite(.was(.added  (fav))): output(.dashboard(.favoriteAdded  (fav, .succeeded)))
        case let .favorite(.was(.removed(fav))): output(.dashboard(.favoriteRemoved(fav, .succeeded)))
        }
    }
    func process(command c:Message._Dashboard) {
        if case let .add   (fav) = c { request(adder, to: .add   (fav)) }
        if case let .remove(fav) = c { request(adder, to: .remove(fav)) }
    }
    return { msg in
        if case let .dashboard(cmd) = msg { process(command:cmd)}
    }
}
// MARK: - Executable Documentation
#if targetEnvironment(simulator)
enum DashboardFeature {
    static func Documentation ()  -> [(Message._Dashboard,Message._Dashboard,Message._Dashboard)]{
        let f = Favorite(kind: .switchOnOff, lightID: "01")
        let e = LightError.unknown
        let docs: [(Message._Dashboard,Message._Dashboard,Message._Dashboard)] =
        [ //  |-- CMD --|   |------ Response Success -----|   |----- Response Failure -----|
            ( .add   (f),   .favoriteAdded  (f,.succeeded),   .favoriteAdded  (f,.failed(e)) ),
            ( .remove(f),   .favoriteRemoved(f,.succeeded),   .favoriteRemoved(f,.failed(e)) )
        ]
        return docs
    }
}
#endif
