//
//  FavoriteAdder.swift
//  BrighterHue
//
//  Created by vikingosegundo on 17/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel

func request(_ adder:FavoriteAdder, to r:FavoriteAdder.Request) { adder.request(r) }

struct FavoriteAdder: UseCase {
    enum Request  { case add(Favorite), remove(Favorite) }
    enum Response {
        case favorite(_Fav); enum _Fav {
            case was (_Was); enum _Was {
                case added  (Favorite)
                case removed(Favorite) } }
    }
    
    init(store: AppStore, responder: @escaping (Response) -> ()) {
        self.store      = store
        self.respond    = responder
    }
    
    func request(_ request:Request) {
        switch request {
        case let .add   (f): store.change(.by(.adding  (.favorite(f)))); respond(.favorite(.was(.added  (f))))
        case let .remove(f): store.change(.by(.removing(.favorite(f)))); respond(.favorite(.was(.removed(f))))
        }
    }
    
    private let store     : AppStore
    private let respond   : (Response) -> ()
    
    typealias RequestType = Request
    typealias ResponseType = Response
}
