//
//  LogitemWriter.swift
//  BrighterHue
//
//  Created by vikingosegundo on 24/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel

func request(_ writer:LogitemWriter, to r:LogitemWriter.Request) { writer.request(r) }

struct LogitemWriter:UseCase {
    enum Request  { case write(item:LogItem) }
    enum Response {                          }
    
    let f: (LogItem) -> ()
    init(print p: @escaping (LogItem) -> () = { print("\($0.date): \($0.text)")}) {
        f = p
    }
    
    func request(_ request:Request) {
        switch request {
        case let .write(item: item): f(item)
        }
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
}
