//
//  createLoggingFeature.swift
//  BrighterHue
//
//  Created by vikingosegundo on 22/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel


func createLoggingFeature(print p: @escaping (LogItem) -> () = { print("\($0.date): \($0.text)")}) -> Input {
    let writer = LogitemWriter(print: p)
    func execute(command cmd:Message._Logging) {
        switch cmd {
        case let .write(item): request(writer, to:.write(item:item))
        }
    }
    return { // entry point
        if case .logging(let c) = $0 { execute(command:c) }
    }
}
