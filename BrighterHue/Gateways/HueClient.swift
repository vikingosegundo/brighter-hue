//
//  HueClient.swift
//  BrighterHue
//
//  Created by vikingosegundo on 18/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel

final
class HueClient:NetworkClient {
    init(ip: String, user: String) {
        self.ip = ip
        self.user = user
    }
    func lights(_ callback: @escaping (Result<[Light], LightError>) -> ()) {
        if let url = URL(string: "http://\(ip)/api/\(user)/lights") {
            let task = URLSession(configuration: .default).dataTask(with: URLRequest(url: url)) { data, _, error in
                switch error {
                case .some(let err): callback(.failure(.lightsEndpoint(err)))
                case .none:          callback(.success(getLights(from: data!)))
                }
            }
            task.resume()
        } else {
            callback(.failure(.lightsEndpoint(nil)))
        }
    }
    
    func set(values: Light.Values, on light: Light,  _ callBack: @escaping (Result<Light, LightError>) -> ()) {
        let params: [String: Any]
        
        switch values {
        case let .hsb(hue, sat, bri):
            params = [ "hue": Int(hue * Double((1 << 16) - 1)),
                       "sat": Int(sat * Double((1 <<  8) - 2)),
                       "bri": Int(bri * Double((1 <<  8) - 2)) ]
        case let .ct(c, bri):
            params = [ "ct" : c,
                       "bri": Int(bri * Double((1 << 8) - 2)) ]
        case let .bri(bri):
            params = [ "bri": Int(bri * Double((1 << 8) - 2)) ]
        }
        
        let url = URL(string: "http://\(ip)/api/\(user)/lights/\(light.id)/state")
        if let data = try? JSONSerialization.data(withJSONObject: params), let url = url {
            let request = NSMutableURLRequest(url: url as URL)
            request.httpBody = data
            request.httpMethod = "PUT"
            let task = URLSession(configuration: .default).dataTask(with: request as URLRequest) { _, _, error in
                switch error {
                case .some(let e): callBack(.failure(.lightsEndpoint(e)))
                case .none:
                    let newLight: Light
                    switch values {
                    case let .hsb(h,s,b): newLight = alter(light, by:.setting(.hue(to:h)), .setting(.saturation(to:s)), .setting(.brightness(to:b)))
                    case let .ct ( c, b): newLight = alter(light, by:.setting(.temperature(to:c.mk)),                   .setting(.brightness(to:b)))
                    case let .bri(    b): newLight = alter(light, by:                                                   .setting(.brightness(to:b)))
                    }
                    callBack(.success(newLight))
                }
            }
            task.resume()
        }
    }
    func toggle(light: Light,       _ onOff: Turn, _ callBack: @escaping (Result<Light, LightError>) -> ()) {
        switch onOff{
        case .on : on (light, callBack)
        case .off: off(light, callBack)
        }
    }
    func rooms(_ callback: @escaping (Result<[Room], LightError>) -> ()) {
        if let url = URL(string: "http://\(ip)/api/\(user)/groups") {
            let task = URLSession(configuration: .default).dataTask(with: URLRequest(url: url)) { data, _, error in
                switch error {
                case .some(let err): callback(.failure(.roomsEndpoint(err)))
                case .none:          callback(.success(getRooms(from: data ?? Data())))
                }
            }
            task.resume()
        } else {
            callback(.failure(.roomsEndpoint(nil)))
        }
    }
    func rename(light: Light, to name: String, _ callback: @escaping (Result<Light, LightError>) -> ()) { callback(.success(alter(light, by:.renaming(.it(to:name))))) }
    
    private let user: String
    private let ip: String
    
    private func on (_ light: Light, _ callBack: @escaping (Result<Light, LightError>) -> ()) { onOff(light: light, callBack, .on ) }
    private func off(_ light: Light, _ callBack: @escaping (Result<Light, LightError>) -> ()) { onOff(light: light, callBack, .off) }
    private func onOff(light: Light, _ callBack: @escaping (Result<Light, LightError>) -> (), _ onOff:Switch) {
        let url = URL(string: "http://\(ip)/api/\(user)/lights/\(light.id)/state")
        let params = ["on": onOff == .on]
        if let data = try? JSONSerialization.data(withJSONObject: params), let url = url {
            let request = NSMutableURLRequest(url: url as URL)
            request.httpBody = data
            request.httpMethod = "PUT"
            let task = URLSession(configuration: .default).dataTask(with: request as URLRequest) { _, _, error in
                switch error {
                case .some(let e): callBack(.failure(.lightsEndpoint(e)))
                case .none: callBack(.success(light.alter(by: .turning(.it(onOff == .on ? .on : .off)))))
                }
            }
            task.resume()
        }
    }
}
fileprivate
func getRooms(from data:Data) -> [Room] {
    guard
        let x = try? JSONSerialization.jsonObject(with: data) as? [String:[String:Any]]
    else { return [] }
    return x.map { room(from:$0.1, for:$0.0)}
}
fileprivate
func getLights(from data: Data) -> [Light] {
    guard
        let x = try? JSONSerialization.jsonObject(with: data) as? [String:[String:Any]]
    else { return [] }
    return x.map { light(from:$0.1, for:$0.0)}
}
fileprivate
func light(from val: [String: Any], for key: String) -> Light {
    let hue = ((val["state"] as? [String:Any])?["hue"] as? Int)
    let ct  = ((val["state"] as? [String:Any])?["ct" ] as? Int)
    let bri = ((val["state"] as? [String:Any])?["bri"] as? Int)
    let light: Light
    if hue != nil {
        light = hueLight(from: val, for: key)
    } else {
        light = dimmabaleLight(from: val, for: key)
    }
    switch (hue, ct, bri) {
    case (.some(_),.some(_) ,.some(_)): return alter(light, by:.adding(.mode(.hsb)), .adding(.mode(.ct)))
    case (.some(_),.some(_) ,.none   ): return alter(light, by:.adding(.mode(.hsb)), .adding(.mode(.ct)))
    case (.none,   .some(_) ,.some(_)): return alter(light, by:                      .adding(.mode(.ct)))
    default: return light
    }
}
fileprivate
func dimmabaleLight(from val: [String: Any], for key: String) -> Light {
    alter(Light(id: key, name: (val["name"] as! String)),
          by:
            .turning(.it        (         ((val["state"] as! [String:Any])["on" ] as! Bool) ? .on : .off           )),
            .setting(.brightness(to:Double((val["state"] as! [String:Any])["bri"] as! Int ) / Double((1 <<  8) - 2)))
    )
}

fileprivate
func hueLight(from val: [String: Any], for key: String) -> Light {
    alter(Light(id: key, name: (val["name"] as! String)),
          by:
            .turning(.it        (         ((val["state"] as! [String:Any])["on" ] as! Bool) ? .on : .off           )),
            .setting(.hue       (to:Double((val["state"] as! [String:Any])["hue"] as! Int ) / Double((1 << 16) - 1))),
            .setting(.saturation(to:Double((val["state"] as! [String:Any])["sat"] as! Int ) / Double((1 <<  8) - 2))),
            .setting(.brightness(to:Double((val["state"] as! [String:Any])["bri"] as! Int ) / Double((1 <<  8) - 2)))
        )
}
fileprivate
func room(from val: [String: Any], for key: String) -> Room { Room(id:key,title: val["name"] as! String) }
