//
//  NetworkClient.swift
//  BrighterHue
//
//  Created by vikingosegundo on 18/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel

protocol NetworkClient {
    func lights(                                       _ callback: @escaping (Result<[Light],LightError>) -> ())
    func rooms (                                       _ callback: @escaping (Result<[Room], LightError>) -> ())
    func set   (values: Light.Values, on light: Light, _ callBack: @escaping (Result<Light,  LightError>) -> ())
    func toggle( light: Light, _ onOff: Turn,          _ callBack: @escaping (Result<Light,  LightError>) -> ())
    func rename( light: Light, to name: String,        _ callback: @escaping (Result<Light,  LightError>) -> ())
}
