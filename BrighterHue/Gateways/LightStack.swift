//
//  HueStack.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Foundation
import BrighterModel
import UIKit

enum LightError: Error {
    case lightsEndpoint(Error?)
    case roomsEndpoint(Error?)
    case unknown
}

enum Switch {
    case on
    case off
}

final
class LightsStack {
    init(client:NetworkClient) { self.client = client }
    func lights(                                    _ callback: @escaping (Result<[Light], LightError>) -> ()) { client.lights(callback) }
    func set(values: Light.Values, on light: Light, _ callBack: @escaping (Result< Light,  LightError>) -> ()) { client.set(values: values, on: light, callBack) }
    func toggle(light: Light, _ onOff: Turn,        _ callBack: @escaping (Result< Light,  LightError>) -> ()) { client.toggle(light: light, onOff, callBack) }
    func rooms(                                     _ callback: @escaping (Result<[Room],  LightError>) -> ()) { client.rooms(callback) }
    func rename(light: Light, to name: String,      _ callback: @escaping (Result< Light,  LightError>) -> ()) { client.rename(light: light, to: name, callback) }
    private
    let client:NetworkClient
}
