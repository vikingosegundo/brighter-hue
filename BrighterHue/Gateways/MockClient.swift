//
//  MockClient.swift
//  BrighterHue
//
//  Created by vikingosegundo on 18/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BrighterModel

final
class MockClient: NetworkClient {
    init(with lights: [Light], rooms:[Room]) {
        self.lights = lights
        self.rooms = rooms
    }
    var shouldFail = false
    private var lights:[Light]
    private var rooms:[Room]
    
    func lights(_ callback: @escaping (Result<[Light], LightError>) -> ()) {
        guard shouldFail == false else { callback(.failure(.lightsEndpoint(nil))); return }
        callback(.success(lights))
    }
    
    func set(values: Light.Values, on light: Light, _ callBack: @escaping (Result<Light, LightError>) -> ()) {
        guard shouldFail == false else { callBack(.failure(LightError.unknown)); return }
        switch values {
        case let .hsb(h,s,b): lights = lights.filter{$0.id != light.id} + [ light.alter(by: .setting(.hue(to: h)), .setting(.saturation(to: s)), .setting(.brightness(to: b)))]
        case let .bri(b): lights = lights.filter{$0.id != light.id} + [ light.alter(by: .setting(.brightness(to: b)))]
        case let .ct(t, b): lights = lights.filter{$0.id != light.id} + [ light.alter(by: .setting(.brightness(to: b)), .setting(.temperature(to: .mirek(t))))]
        }
        callBack(.success(lights.first(where: {$0.id == light.id})!))
    }
    
    func toggle(light: Light, _ onOff: Turn, _ callBack: @escaping (Result<Light, LightError>) -> ()) {
        guard shouldFail == false else { callBack(.failure(LightError.unknown)); return }
        lights = lights.filter{ $0.id != light.id } + [light.alter(by: .turning(.it(onOff == .on ? .on : .off)))]
        callBack(.success(lights.first(where: {$0.id == light.id})!))
    }
    
    func rooms(_ callback: @escaping (Result<[Room], LightError>) -> ()) {
        guard shouldFail == false else { callback(.failure(.lightsEndpoint(nil))); return }
        callback(.success(rooms))
    }
    
    func rename(light: Light, to name: String, _ callback: @escaping (Result<Light, LightError>) -> ()) {
        guard shouldFail == false else { callback(.failure(.lightsEndpoint(nil))); return }
        lights = lights.filter{ $0.id != light.id } + [light.alter(by: .renaming(.it(to: name)))]
        callback(.success(lights.first(where: {$0.id == light.id})!))
    }
}
