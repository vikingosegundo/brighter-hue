//
//  Collection+KeyPathSorting.swift
//  Todos
//
//  Created by Manuel Meyer on 19/09/2020.
//

import Foundation
import SwiftUI

public func last <Element>(of array: [Element]) -> Element? { array.last  }
public func first<Element>(of array: [Element]) -> Element? { array.first }

public func createArray<Element>(by isIncluded: (Element) throws -> Bool, from array:[Element] ) -> [Element] {
    array.filter {
        do {
            return try isIncluded($0)
        } catch { return false }
    }
}

extension Collection {
    public func sorted<Value: Comparable>(on property: KeyPath<Element, Value>, by areInIncreasingOrder: (Value, Value) -> Bool) -> [Element] {
        sorted { currentElement, nextElement in
            areInIncreasingOrder(currentElement[keyPath: property], nextElement[keyPath: property])
        }
    }
}

public
extension Date {
    static var     today: Date { Date().midnight      }
    static var yesterday: Date { Date.today.dayBefore }
    static var  tomorrow: Date { Date.today.dayAfter  }
    
    var dayBefore: Date { cal.date(byAdding: .day, value: -1, to: self)! }
    var  dayAfter: Date { cal.date(byAdding: .day, value:  1, to: self)! }
    var  midnight: Date { cal.date(bySettingHour:  0, minute: 0, second: 0, of: self)!}
    var      noon: Date { cal.date(bySettingHour: 12, minute: 0, second: 0, of: self)!}
    
    private var cal: Calendar { Calendar.current }
}

public func humanDate(_ date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "hMdmY", options: 0, locale: Locale.current)
    return formatter.string(from: date)
}

public extension UIColor {
    typealias RGBA = (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)
    
    var rgbaValues: RGBA  {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r,
                    green: &g,
                    blue : &b,
                    alpha: &a
        )
        return (red:r, green:g, blue:b, alpha: a)
    }
    
    convenience init(hue h: CGFloat, saturation s: CGFloat, lightness l: CGFloat, alpha : CGFloat) {
        let v = UIColor
            .from(hue       : h,
                  saturation: s,
                  lightness : l,
                  alpha     : alpha)
            .rgbaValues
        self.init(red   : v.red,
                  green : v.green,
                  blue  : v.blue,
                  alpha : v.alpha
        )
    }
    
    static func from(hue h: CGFloat, saturation s: CGFloat, lightness l: CGFloat, alpha a: CGFloat = 1.0) -> UIColor {
        precondition(0...1 ~= h && 0...1 ~= s && 0...1 ~= l && 0...1 ~= a, "input range must be in 0...1" )
        let h6: CGFloat = (h * 6.0).truncatingRemainder(dividingBy: 6.0)
        
        let c: CGFloat = (1 - abs((2 * l) - 1)) * s
        let x: CGFloat = c * (1 - abs(h6.truncatingRemainder(dividingBy: 2) - 1))
        let m: CGFloat = l - (c / 2)
        
        func rgbaFrom(_ r: CGFloat, _ g: CGFloat, _ b: CGFloat) -> RGBA { (red: r+m, green: g+m, blue: b+m, alpha: a) }
        func color(from rgba: RGBA) -> UIColor { .init(red: rgba.red, green: rgba.green, blue: rgba.blue, alpha: rgba.alpha) }
        
        switch h6 {
        case 0..<1: return color(from:rgbaFrom(c,x,0))
        case 1..<2: return color(from:rgbaFrom(x,c,0))
        case 2..<3: return color(from:rgbaFrom(0,c,x))
        case 3..<4: return color(from:rgbaFrom(0,x,c))
        case 4..<5: return color(from:rgbaFrom(x,0,c))
        case 5..<6: return color(from:rgbaFrom(c,0,x))
        default:    return .black
        }
    }
    
    convenience init(kelvin: Double) {
        let temperature = kelvin / 100.0
        let red, green, blue: Double
//        red
        if (temperature < 66.0) { red = 255
        } else                  { let r = temperature - 55.0; red   = clip( 351.97690566805693 + 0.114206453784165   * r - 40.253663093321270 * log(r)) }
//        green
        if (temperature < 66.0) { let g = temperature -  2.0; green = clip(-155.25485562709179 - 0.44596950469579133 * g + 104.49216199393888 * log(g))
        } else                  { let g = temperature - 50.0; green = clip( 325.44941257119740 + 0.07943456536662342 * g -  28.08529635079570 * log(g)) }
//        blue
        if (temperature >= 66.0) { blue = 255
        } else {
            if (temperature <= 20.0) { blue = 0
            } else                   { let b = temperature - 10; blue = clip(-254.76935184120902 + 0.8274096064007395 * b + 115.67994401066147 * log(b))
            }
        }
        self.init(red: CGFloat(red / 255.0), green: CGFloat(green / 255.0), blue: CGFloat(blue / 255.0), alpha: 1)
    }
}
func clip(_ value: Double) -> Double { min(max(value, 0), 255) }
