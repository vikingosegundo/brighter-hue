//
//  BrighterTools.h
//  BrighterTools
//
//  Created by vikingosegundo on 22/07/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for BrighterTools.
FOUNDATION_EXPORT double BrighterToolsVersionNumber;

//! Project version string for BrighterTools.
FOUNDATION_EXPORT const unsigned char BrighterToolsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BrighterTools/PublicHeader.h>


